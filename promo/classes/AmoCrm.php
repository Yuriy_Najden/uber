<?php

class AmoCrm
{
    private $user = [];
    private $subDomain;

    /**
     * AmoCrm constructor.
     * @param array $user
     * @param $subDomain
     */
    public function __construct(array $user, $subDomain)
    {
        $this->user = $user;
        $this->subDomain = $subDomain;

        $this->curlInit('https://'. $this->subDomain . '.amocrm.ru/private/api/auth.php?type=json', $user);
    }

    /**
     * @param $link
     * @param $data
     * @return mixed
     */
    private function curlInit($link, $data)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return json_decode($out, true);
    }

    /**
     * @param $title
     * @param $statusId
     * @param $utmSource
     * @param $utmMedium
     * @param $utmCampaign
     * @param $utmTerm
     * @param $utmContent
     * @param $package
     * @param $instacaunt
     * @param $country
     * @param $question
     * @return mixed
     */
    public function storeLead($title, $statusId, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent, $package, $instacaunt, $country, $question)
    {

        $leads['request']['leads']['add'] = array(
            array(
                'name'        => $title,
                'date_create' => time(),
                'status_id'   => $statusId,
                'custom_fields' => [
                    [
                        'id'     => 386169,
                        'values' => [
                            [
                                'value' => $utmSource,
                            ],
                        ],
                    ],
                    [
                        'id'     => 386171,
                        'values' => [
                            [
                                'value' => $utmMedium,
                            ],
                        ],
                    ],
                    [
                        'id'     => 386173,
                        'values' => [
                            [
                                'value' => $utmCampaign,
                            ],
                        ],
                    ],
                    [
                        'id'     => 386175,
                        'values' => [
                            [
                                'value' => $utmTerm,
                            ],
                        ],
                    ],
                    [
                        'id'     => 386177,
                        'values' => [
                            [
                                'value' => $utmContent,
                            ],
                        ],
                    ],
                    [
                        'id'     => 386129,
                        'values' => [
                            [
                                'value' => $package,
                            ],
                        ],
                    ],
                    [
                        'id'     => 386139,
                        'values' => [
                            [
                                'value' => $instacaunt,
                            ],
                        ],
                    ],
                    [
                        'id'     => 386291,
                        'values' => [
                            [
                                'value' => $country,
                            ],
                        ],
                    ],
                    [
                        'id'     => 386381,
                        'values' => [
                            [
                                'value' => $question,
                            ],
                        ],
                    ],
                ],

            )

        );

        return $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/v2/json/leads/set', $leads);
    }

    /**
     * @param $leadId
     * @param $statusId
     * @return mixed
     */
    public function updateLead($leadId, $statusId)
    {
        $leads['request']['leads']['update'] = [
            [
                'id'            => $leadId,
                'last_modified' => time(),
                'status_id'     => $statusId
            ]
        ];

        return $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/v2/json/leads/set', $leads);
    }

    /**
     * @param $name
     * @param $leadId
     * @param $email
     * @param $phone
     * @return mixed
     */
    public function storeContact($name, $leadId, $email, $phone)
    {
        $contacts['request']['contacts']['add'] = [
            [
                'name'            => $name,
                'linked_leads_id' => [
                    $leadId
                ],
                'custom_fields'   => [
                    [
                        // Phones
                        'id'     => 361887,
                        'values' => [
                            [
                                'value' => $phone,
                                'enum'  => 'MOB',
                            ],
                        ],
                    ],
                    [
                        //Emails
                        'id'     => 361889,
                        'values' => [
                            [
                                'value' => $email,
                                'enum'  => 'PRIV',
                            ],
                        ],
                    ]
                ]
            ],
        ];

        return $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/v2/json/contacts/set', $contacts);
    }

}