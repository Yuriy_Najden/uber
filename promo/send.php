<?php
session_start();

require_once ("vendor/autoload.php");

require_once('classes/AmoCrm.php');

use PHPMailer\PHPMailer\PHPMailer;

/**
 * @param $data
 * @return string
 */
function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$name = clearData($_POST['name']);
$phone = clearData($_POST['phone']);
$email = clearData($_POST['email']);
$instacaunt = clearData($_POST['instacaunt']);
$package = clearData($_POST['package']);

$utmSource = clearData($_SESSION['utm_source']);
$utmMedium = clearData($_SESSION['utm_medium']);
$utmCampaign = clearData($_SESSION['utm_campaign']);
$utmTerm = clearData($_SESSION['utm_term']);
$utmContent = clearData($_SESSION['utm_content']);

if(!empty($name) && !empty($phone)) {

    //**// Save user in crm
    $amoCrm = new AmoCrm([
        'USER_LOGIN' => 'info@projectx.fit',
        'USER_HASH'  => '17d8f036a67bd9a18945d718b8aa19fc'
    ], 'infoprojectxfit');

    $lead = $amoCrm->storeLead('Забронировать место', 20081497, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent, $package, $instacaunt, $country, $question);

    $leadId = $lead['response']['leads']['add'][0]['id'];

    $amoCrm->storeContact($name, $leadId, $email, $phone);

    $mail = new PHPMailer();

    try {

         //Server settings
        $mail->isSMTP();
        $mail->Host = 'mail.adm.tools';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@projectx.fit';
        $mail->Password = '405E9tPh40us';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 25;
        $mail->CharSet = 'UTF-8';

        //Recipients
        $mail->setFrom('info@projectx.fit', 'info');
        $mail->addAddress('info@projectx.fit', 'info');

        //Content
        $mail->isHTML(true);
        $mail->Subject = 'Забронировать место';
        $mail->Body = "<p>$package</p><p>Имя: $name</p><p>Телефон: $phone</p><p>Email: $email</p><p>Аккаунт в Instagram: $instacaunt</p>";

        $mail->send();

    } catch (Exception $e) {
        echo 'Message could not be sent.';

        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }

    header('Location: mail-thx.php');

} else {

    die('Data is empty!');

}

