<div class="modal fade my-popups" id="pravicy" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Политика конфиденциальности</h4>
            </div>
            <div class="modal-body">
                <p>Ваша конфиденциальность очень важна для нас. Мы хотим, чтобы Ваша работа в Интернет по возможности
                    была максимально приятной и полезной, и Вы совершенно спокойно использовали широчайший спектр
                    информации, инструментов и возможностей, которые предлагает Интернет.
                </p>

                <p>Личная информация уччастников марафона, собранная при регистрации (или в любое другое время)
                    преимущественно используется для подготовки Продуктов или Услуг в соответствии с Вашими потребностями.
                    Ваша информация не будет передана или продана третьим сторонам. Фотографии, до и после, участников
                    марафона могут быть использованы нами как пример отличной работы в наших марафонах (лицо будет закрыто)
                </p>

                <h3>Какие данные собираются на сайте</h3>

                <p>При добровольной регистрации вы отправляете свое Имя, E-mail и Номер телефона через форму регистрации.</p>

                <h3>С какой целью собираются эти данные</h3>

                <p>Имя используется для обращения лично к вам, а ваш e-mail для отправки вам писем рассылок, новостей
                    марафона, полезных материалов, коммерческих предложений.
                    Ваши имя и e-mail не передаются третьим лицам, ни при каких условиях кроме случаев, связанных с
                    исполнением требований законодательства. Ваше имя и e-mail на защищенных серверах сервиса getresponse.com
                    и используются в соответствии с его политикой конфиденциальности.
                </p>

                <p>Вы можете отказаться от получения писем рассылки и удалить из базы данных свои контактные данные в
                    любой момент, кликнув на ссылку для отписки, присутствующую в каждом письме.
                </p>

                <h3>Как эти данные используются</h3>

                <p>На сайте используются куки (Cookies) и данные о посетителях сервиса Google Analytics.</p>

                <p>При помощи этих данных собирается информация о действиях посетителей на сайте с целью улучшения его
                    содержания, улучшения функциональных возможностей сайта и, как следствие, создания качественного контента
                    и сервисов для посетителей.
                </p>

                <p>Вы можете в любой момент изменить настройки своего браузера так, чтобы браузер блокировал все файлы
                    cookie или оповещал об отправке этих файлов. Учтите при этом, что некоторые функции и сервисы не смогут
                    работать должным образом.
                </p>

                <h3>Как эти данные защищаются</h3>

                <p>Для защиты Вашей личной информации мы используем разнообразные административные, управленческие и
                    технические меры безопасности. Наша Компания придерживается различных международных стандартов контроля,
                    направленных на операции с личной информацией, которые включают определенные меры контроля по защите
                    информации, собранной в Интернет.
                </p>

                <p>Наших сотрудников обучают понимать и выполнять эти меры контроля, они ознакомлены с нашим Уведомлением
                    о конфиденциальности, нормами и инструкциями.
                </p>

                <p>Тем не менее, несмотря на то, что мы стремимся обезопасить Вашу личную информацию, Вы тоже должны
                    принимать меры, чтобы защитить ее.
                </p>

                <p>Мы настоятельно рекомендуем Вам принимать все возможные меры предосторожности во время пребывания в
                    Интернете. Организованные нами услуги и веб-сайты предусматривают меры по защите от утечки,
                    несанкционированного использования и изменения информации, которую мы контролируем. Несмотря на то,
                    что мы делаем все возможное, чтобы обеспечить целостность и безопасность своей сети и систем, мы не
                    можем гарантировать, что наши меры безопасности предотвратят незаконный доступ к этой информации
                    хакеров сторонних организаций.
                </p>

                <p>В случае изменения данной политики конфиденциальности вы сможете прочитать об этих изменениях на этой
                    странице или, в особых случаях, получить уведомление на свой e-mail.
                </p>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>