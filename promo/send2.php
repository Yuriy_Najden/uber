<?php
session_start();

require_once("classes/AmoCrm.php");

require_once ("vendor/autoload.php");

use PHPMailer\PHPMailer\PHPMailer;

/**
 * @param $data
 * @return string
 */
function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$instacaunt = clearData($_POST['instacaunt']);
$package = clearData($_POST['package']);
$email = clearData($_POST['email']);
$phone = clearData($_POST['phone']);
$name = clearData($_POST['name']);
$country = clearData($_POST['country']);

$utmSource = clearData($_SESSION['utm_source']);
$utmMedium = clearData($_SESSION['utm_medium']);
$utmCampaign = clearData($_SESSION['utm_campaign']);
$utmTerm = clearData($_SESSION['utm_term']);
$utmContent = clearData($_SESSION['utm_content']);

if(!empty($instacaunt)) {

    // Save user in crm
    $amoCrm = new AmoCrm([
        'USER_LOGIN' => 'info@projectx.fit',
        'USER_HASH'  => '17d8f036a67bd9a18945d718b8aa19fc'
    ], 'infoprojectxfit');

    $lead = $amoCrm->storeLead('Подтверждённая оплата', 20081500, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent, $package, $instacaunt, $country, $question);

    $leadId = $lead['response']['leads']['add'][0]['id'];

    $amoCrm->storeContact($name, $leadId, $email, $phone);

    $mail = new PHPMailer();

    try {

        //Server settings
        $mail->isSMTP();
        $mail->Host = 'mail.adm.tools';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@projectx.fit';
        $mail->Password = '4O5E9tPh4Ous';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 25;
        $mail->CharSet = 'UTF-8';

        //Recipients
        $mail->setFrom('info@projectx.fit', 'info');
        $mail->addAddress('info@projectx.fit', 'info');

        //Content
        $mail->isHTML(true);
        $mail->Subject = 'Подтверждённая оплата';
        $mail->Body = "<p>Имя: $name</p><p>Аккаунт в Instagram: $instacaunt</p><p>$package</p><p>Email: $email</p>
                       <p>Телефон: $phone</p><p>Страна: $country</p>";

        $mail->send();

    } catch (Exception $e) {
        echo 'Message could not be sent.';

        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }

    header('Location: index.php');

} else {

    die('Data is empty!');

}