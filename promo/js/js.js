$(function() {

    
    //SCROLL MENU

   $(document).on('click', '.scroll-to', function (e) {
       e.preventDefault()
       var href = $(this).attr('href')
       $('html, body').animate({
           scrollTop: $(href).offset().top
       }, 1000)
   })
    

    // CASTOME SLIDER ARROWS

   $('#res-slider').slick({
       autoplay: true,
       autoplaySpeed: 3000,
       slidesToShow: 2,
       slidesToScroll: 2,
       arrows: false,
       fade: false,
       responsive: [
           {
               breakpoint:440,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1
               }
           }
       ]

   });

   $('.res-wrapper .prev').click(function(e){
       e.preventDefault();
       $('#res-slider').slick('slickPrev');
   });

   $('.res-wrapper .next').click(function(e){
       e.preventDefault();
       $('#res-slider').slick('slickNext');
   });

   //

    $('#rev-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        dots: true,
        adaptiveHeight: true,
        fade: false,
        responsive: [
            {
                breakpoint:992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]

    });

    $('.reviews .prev').click(function(e){
        e.preventDefault();
        $('#rev-slider').slick('slickPrev');
    });

    $('.reviews .next').click(function(e){
        e.preventDefault();
        $('#rev-slider').slick('slickNext');
    });

    //

    $('#mob-rev-slider').slick({
        autoplay: true,
        autoplaySpeed: 15000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        adaptiveHeight: true,
        fade: false

    });

    $('.reviews .prev').click(function(e){
        e.preventDefault();
        $('#mob-rev-slider').slick('slickPrev');
    });

    $('.reviews .next').click(function(e){
        e.preventDefault();
        $('#mob-rev-slider').slick('slickNext');
    });

   

    // DTA VALUE REPLACE

    $('.on-booking').on('click', function (e) {
        e.preventDefault();
        var type = $(this).data('type');
        $('#form-popup-booking').find('input[name=package]').val(type);
    });

    $('.on-pay').on('click', function (e) {
        e.preventDefault();
        var type = $(this).data('type');
        $('.my-pay-form').find('input[name=package]').val(type);
        $('.my-pay-form').find('input[name=formcomment]').val(type);
        $('.my-pay-form').find('input[name=short-dest]').val(type)
    });

    //

    $(".my-pay-form").submit(function() { //Change
        var myakkaunt = $('.my-pay-form').find('input[name=instacaunt]').val();
        var mypackeg = $('.my-pay-form').find('input[name=package]').val();
        var myemail = $('.my-pay-form').find('input[name=email]').val();
        var myphone = $('.my-pay-form').find('input[name=phone]').val();
        var myname = $('.my-pay-form').find('input[name=name]').val();
        var mycountry = $('.my-pay-form').find('input[name=country]').val();

        localStorage.setItem('mykey', myakkaunt);
        localStorage.setItem('mykey2', mypackeg);
        localStorage.setItem('mykey3', myemail);
        localStorage.setItem('mykey4', myphone);
        localStorage.setItem('mykey5', myname);
        localStorage.setItem('mykey6', mycountry);

        var th = $(this);
        $.ajax({
            type: "POST",
            url: "https://smmstudio.com.ua/readymag/projectx/send4.php", //Change
            data: th.serialize()
        });
    });

    var localValue = localStorage.getItem('mykey');
    var localValue2 = localStorage.getItem('mykey2');
    var localValue3 = localStorage.getItem('mykey3');
    var localValue4 = localStorage.getItem('mykey4');
    var localValue5 = localStorage.getItem('mykey5');
    var localValue6 = localStorage.getItem('mykey6');

    $('.confirmation-pay').find('input[name=instacaunt]').val(localValue);
    $('.confirmation-pay').find('input[name=package]').val(localValue2);
    $('.confirmation-pay').find('input[name=email]').val(localValue3);
    $('.confirmation-pay').find('input[name=phone]').val(localValue4);
    $('.confirmation-pay').find('input[name=name]').val(localValue5);
    $('.confirmation-pay').find('input[name=country]').val(localValue6);

    // PHONE MASK

    $("input[type=tel].ukr-mask").mask("+38(999) 999-99-99");
    $("input[type=tel].rus-mask").mask("+7(999) 999-99-99");

    //

    $('.q-title').on('click', function (e) {
        e.preventDefault();
    });

    //

    $(document).on('click', '.play', function(){
        jQuery("iframe").each(function() {
            jQuery(this)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')});
    });
    //

    var now = new Date().getMonth();
    var nowDate = new Date().getDate();
    var nowMonth = 1;
    var timerMonth = 0;

    if(now < 1){
        nowMonth = 1;
    }else {
        nowMonth = now + 1;
    }

    if( nowDate >= 15){
        timerMonth = nowMonth +1;
    } else{
        timerMonth = nowMonth;
    }

    $(document).ready(function () {
        $('#timer-header').countdown('2018/' + timerMonth + '/15 00:00', function (event) {
            $(this).html(event.strftime(''
                + '<div class="v-center">%D<span class="after-number">Дней</span></div>'
                + '<div class="v-center">%H<span class="after-number">Часов</span></div>'
                + '<div class="v-center">%M<span class="after-number">Минут</span></div>'
                + '<div class="v-center">%S<span class="after-number">Секунд</span></div>'));
        });
    });

});

//


//

$(window).on('load', function () {

    'use strict';

    $('#loader').fadeOut(400);



});
