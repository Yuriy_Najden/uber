$(function() {


    //SCROLL MENU

   $(document).on('click', '.scroll-to', function (e) {
       e.preventDefault();
       var href = $(this).attr('href');
       $('html, body').animate({
           scrollTop: $(href).offset().top
       }, 1000)
   });

    // CASTOME SLIDER ARROWS

   $('#res-slider').slick({
       autoplay: true,
       autoplaySpeed: 3000,
       slidesToShow: 2,
       slidesToScroll: 2,
       arrows: false,
       fade: false,
       responsive: [
           {
               breakpoint:440,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1
               }
           }
       ]

   });

   $('.res-wrapper .prev').click(function(e){
       e.preventDefault();
       $('#res-slider').slick('slickPrev');
   });

   $('.res-wrapper .next').click(function(e){
       e.preventDefault();
       $('#res-slider').slick('slickNext');
   });

   //

    $('#rev-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        dots: true,
        adaptiveHeight: true,
        fade: false,
        responsive: [
            {
                breakpoint:992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]

    });

    $('.reviews .prev').click(function(e){
        e.preventDefault();
        $('#rev-slider').slick('slickPrev');
    });

    $('.reviews .next').click(function(e){
        e.preventDefault();
        $('#rev-slider').slick('slickNext');
    });

    //

    $('#mob-rev-slider').slick({
        autoplay: true,
        autoplaySpeed: 15000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        adaptiveHeight: true,
        fade: false

    });

    $('.reviews .prev').click(function(e){
        e.preventDefault();
        $('#mob-rev-slider').slick('slickPrev');
    });

    $('.reviews .next').click(function(e){
        e.preventDefault();
        $('#mob-rev-slider').slick('slickNext');
    });


    // DTA VALUE REPLACE

    $('.on-booking').on('click', function (e) {
        e.preventDefault();
        var type = $(this).data('type');
        $('#form-popup-booking').find('input[name=package]').val(type);
    });

    $('.on-pay').on('click', function (e) {
        e.preventDefault();
        var type = $(this).data('type');
        $('.my-pay-form').find('input[name=package]').val(type);
        $('.my-pay-form').find('input[name=formcomment]').val(type);
        $('.my-pay-form').find('input[name=short-dest]').val(type)

        $('.my-pay-form2').find('input[name=package]').val(type);
        $('.my-pay-form2').find('input[name=formcomment]').val(type);
        $('.my-pay-form2').find('input[name=short-dest]').val(type)

        $('.my-pay-form3').find('input[name=package]').val(type);
        $('.my-pay-form3').find('input[name=formcomment]').val(type);
        $('.my-pay-form3').find('input[name=short-dest]').val(type)

        $('.my-pay-formr').find('input[name=package]').val(type);
        $('.my-pay-formr').find('input[name=formcomment]').val(type);
        $('.my-pay-formr').find('input[name=short-dest]').val(type);

        $('.my-pay-form2r').find('input[name=package]').val(type);
        $('.my-pay-form2r').find('input[name=formcomment]').val(type);
        $('.my-pay-form2r').find('input[name=short-dest]').val(type)

        $('.my-pay-form3r').find('input[name=package]').val(type);
        $('.my-pay-form3r').find('input[name=formcomment]').val(type);
        $('.my-pay-form3r').find('input[name=short-dest]').val(type)
    });

    //

    $(".my-pay-form, .my-pay-form2, .my-pay-form3, .my-pay-formr, .my-pay-form2r, .my-pay-form3r").submit(function() { //Change

        var th = $(this);

        var mypackeg = $(this).find('input[name=package]').val();
        var myemail = $(this).find('input[name=email]').val();
        var myphone = $(this).find('input[name=phone]').val();
        var myname = $(this).find('input[name=name]').val();
        var mycountry = $(this).find('input[name=country]').val();

        Cookies.set('nameX', myname);
        Cookies.set('phoneX', myphone);
        Cookies.set('emailX', myemail);
        Cookies.set('packageX', mypackeg);
        Cookies.set('countryX', mycountry);

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "send4.php", //Change
            data: th.serialize(),
            success: function(rv) {
                if (rv.success) {
                    let $form = $(rv.form);
                    $(document.body).append($form);
                    $form.submit();
                }
            }
        });

        return false;
    });

    /*$('#confirmation-pay').find('input[name=package]').val(Cookies.get('packageX'));
    $('#confirmation-pay').find('input[name=email]').val(Cookies.get('emailX'));
    $('#confirmation-pay').find('input[name=phone]').val(Cookies.get('phoneX'));
    $('#confirmation-pay').find('input[name=name]').val(Cookies.get('nameX'));
    $('#confirmation-pay').find('input[name=country]').val(Cookies.get('countryX'));
*/
    //

    /*$(".my-pay-form2").submit(function() { //Change
        var myakkaunt2 = $('.my-pay-form2').find('input[name=instacaunt]').val();
        var mypackeg2 = $('.my-pay-form2').find('input[name=package]').val();
        var myemail2 = $('.my-pay-form2').find('input[name=email]').val();
        var myphone2 = $('.my-pay-form2').find('input[name=phone]').val();
        var myname2 = $('.my-pay-form2').find('input[name=name]').val();
        var mycountry2 = $('.my-pay-form2').find('input[name=country]').val();

        localStorage.setItem('mykeyf2', myakkaunt2);
        localStorage.setItem('mykey2f2', mypackeg2);
        localStorage.setItem('mykey3f2', myemail2);
        localStorage.setItem('mykey4f2', myphone2);
        localStorage.setItem('mykey5f2', myname2);
        localStorage.setItem('mykey6f2', mycountry2);

        var th = $(this);
        $.ajax({
            type: "POST",
            url: "send4.php", //Change
            data: th.serialize()
        });
    });

    var localValuef2 = localStorage.getItem('mykeyf2');
    var localValue2f2 = localStorage.getItem('mykey2f2');
    var localValue3f2 = localStorage.getItem('mykey3f2');
    var localValue4f2 = localStorage.getItem('mykey4f2');
    var localValue5f2 = localStorage.getItem('mykey5f2');
    var localValue6f2 = localStorage.getItem('mykey6f2');

    $('#confirmation-pay').find('input[name=instacaunt]').val(localValuef2);
    $('#confirmation-pay').find('input[name=package]').val(localValue2f2);
    $('#confirmation-pay').find('input[name=email]').val(localValue3f2);
    $('#confirmation-pay').find('input[name=phone]').val(localValue4f2);
    $('#confirmation-pay').find('input[name=name]').val(localValue5f2);
    $('#confirmation-pay').find('input[name=country]').val(localValue6f2);

    //

    $(".my-pay-form3").submit(function() { //Change
        var myakkaunt3 = $('.my-pay-form3').find('input[name=instacaunt]').val();
        var mypackeg3 = $('.my-pay-form3').find('input[name=package]').val();
        var myemail3 = $('.my-pay-form3').find('input[name=email]').val();
        var myphone3 = $('.my-pay-form3').find('input[name=phone]').val();
        var myname3 = $('.my-pay-form3').find('input[name=name]').val();
        var mycountry3 = $('.my-pay-form3').find('input[name=country]').val();

        localStorage.setItem('mykeyf3', myakkaunt3);
        localStorage.setItem('mykey2f3', mypackeg3);
        localStorage.setItem('mykey3f3', myemail3);
        localStorage.setItem('mykey4f3', myphone3);
        localStorage.setItem('mykey5f3', myname3);
        localStorage.setItem('mykey6f3', mycountry3);

        var th = $(this);
        $.ajax({
            type: "POST",
            url: "send4.php", //Change
            data: th.serialize()
        });
    });

    var localValuef3 = localStorage.getItem('mykeyf3');
    var localValue2f3 = localStorage.getItem('mykey2f3');
    var localValue3f3 = localStorage.getItem('mykey3f3');
    var localValue4f3 = localStorage.getItem('mykey4f3');
    var localValue5f3 = localStorage.getItem('mykey5f3');
    var localValue6f3 = localStorage.getItem('mykey6f3');

    $('#confirmation-pay').find('input[name=instacaunt]').val(localValuef3);
    $('#confirmation-pay').find('input[name=package]').val(localValue2f3);
    $('#confirmation-pay').find('input[name=email]').val(localValue3f3);
    $('#confirmation-pay').find('input[name=phone]').val(localValue4f3);
    $('#confirmation-pay').find('input[name=name]').val(localValue5f3);
    $('#confirmation-pay').find('input[name=country]').val(localValue6f3);

    //

    $(".my-pay-formr").submit(function() { //Change
        var myakkauntr = $('.my-pay-formr').find('input[name=instacaunt]').val();
        var mypackegr = $('.my-pay-formr').find('input[name=package]').val();
        var myemailr = $('.my-pay-formr').find('input[name=email]').val();
        var myphoner = $('.my-pay-formr').find('input[name=phone]').val();
        var mynamer = $('.my-pay-formr').find('input[name=name]').val();
        var mycountryr = $('.my-pay-formr').find('input[name=country]').val();

        localStorage.setItem('mykeyr', myakkauntr);
        localStorage.setItem('mykey2r', mypackegr);
        localStorage.setItem('mykey3r', myemailr);
        localStorage.setItem('mykey4r', myphoner);
        localStorage.setItem('mykey5r', mynamer);
        localStorage.setItem('mykey6r', mycountryr);

        var th = $(this);
        $.ajax({
            type: "POST",
            url: "send4.php", //Change
            data: th.serialize()
        });
    });

    var localValuer = localStorage.getItem('mykeyr');
    var localValue2r = localStorage.getItem('mykey2r');
    var localValue3r = localStorage.getItem('mykey3r');
    var localValue4r = localStorage.getItem('mykey4r');
    var localValue5r = localStorage.getItem('mykey5r');
    var localValue6r = localStorage.getItem('mykey6r');

    $('#confirmation-pay').find('input[name=instacaunt]').val(localValuer);
    $('#confirmation-pay').find('input[name=package]').val(localValue2r);
    $('#confirmation-pay').find('input[name=email]').val(localValue3r);
    $('#confirmation-pay').find('input[name=phone]').val(localValue4r);
    $('#confirmation-pay').find('input[name=name]').val(localValue5r);
    $('#confirmation-pay').find('input[name=country]').val(localValue6r);

    //

    $(".my-pay-form2r").submit(function() { //Change
        var myakkaunt2r = $('.my-pay-form2r').find('input[name=instacaunt]').val();
        var mypackeg2r = $('.my-pay-form2r').find('input[name=package]').val();
        var myemail2r = $('.my-pay-form2r').find('input[name=email]').val();
        var myphone2r = $('.my-pay-form2r').find('input[name=phone]').val();
        var myname2r = $('.my-pay-form2r').find('input[name=name]').val();
        var mycountry2r = $('.my-pay-form2r').find('input[name=country]').val();

        localStorage.setItem('mykeyf2r', myakkaunt2r);
        localStorage.setItem('mykey2f2r', mypackeg2r);
        localStorage.setItem('mykey3f2r', myemail2r);
        localStorage.setItem('mykey4f2r', myphone2r);
        localStorage.setItem('mykey5f2r', myname2r);
        localStorage.setItem('mykey6f2r', mycountry2r);

        var th = $(this);
        $.ajax({
            type: "POST",
            url: "send4.php", //Change
            data: th.serialize()
        });
    });

    var localValuef2r = localStorage.getItem('mykeyf2r');
    var localValue2f2r = localStorage.getItem('mykey2f2r');
    var localValue3f2r = localStorage.getItem('mykey3f2r');
    var localValue4f2r = localStorage.getItem('mykey4f2r');
    var localValue5f2r = localStorage.getItem('mykey5f2r');
    var localValue6f2r = localStorage.getItem('mykey6f2r');

    $('#confirmation-pay').find('input[name=instacaunt]').val(localValuef2r);
    $('#confirmation-pay').find('input[name=package]').val(localValue2f2r);
    $('#confirmation-pay').find('input[name=email]').val(localValue3f2r);
    $('#confirmation-pay').find('input[name=phone]').val(localValue4f2r);
    $('#confirmation-pay').find('input[name=name]').val(localValue5f2r);
    $('#confirmation-pay').find('input[name=country]').val(localValue6f2r);

    //

    $(".my-pay-form3r").submit(function() { //Change
        var myakkaunt3r = $('.my-pay-form3r').find('input[name=instacaunt]').val();
        var mypackeg3r = $('.my-pay-form3r').find('input[name=package]').val();
        var myemail3r = $('.my-pay-form3r').find('input[name=email]').val();
        var myphone3r = $('.my-pay-form3r').find('input[name=phone]').val();
        var myname3r = $('.my-pay-form3r').find('input[name=name]').val();
        var mycountry3r = $('.my-pay-form3r').find('input[name=country]').val();

        localStorage.setItem('mykeyf3r', myakkaunt3r);
        localStorage.setItem('mykey2f3r', mypackeg3r);
        localStorage.setItem('mykey3f3r', myemail3r);
        localStorage.setItem('mykey4f3r', myphone3r);
        localStorage.setItem('mykey5f3r', myname3r);
        localStorage.setItem('mykey6f3r', mycountry3r);

        var th = $(this);
        $.ajax({
            type: "POST",
            url: "send4.php", //Change
            data: th.serialize()
        });
    });

    var localValuef3r = localStorage.getItem('mykeyf3r');
    var localValue2f3r = localStorage.getItem('mykey2f3r');
    var localValue3f3r = localStorage.getItem('mykey3f3r');
    var localValue4f3r = localStorage.getItem('mykey4f3r');
    var localValue5f3r = localStorage.getItem('mykey5f3r');
    var localValue6f3r = localStorage.getItem('mykey6f3r');

    $('#confirmation-pay').find('input[name=instacaunt]').val(localValuef3r);
    $('#confirmation-pay').find('input[name=package]').val(localValue2f3r);
    $('#confirmation-pay').find('input[name=email]').val(localValue3f3r);
    $('#confirmation-pay').find('input[name=phone]').val(localValue4f3r);
    $('#confirmation-pay').find('input[name=name]').val(localValue5f3r);
    $('#confirmation-pay').find('input[name=country]').val(localValue6f3r);*/

    // PHONE MASK

    $("input[type=tel].ukr-mask").mask("+38(999) 999-99-99");
    $("input[type=tel].rus-mask").mask("+7(999) 999-99-99");

    //

    $('.q-title').on('click', function (e) {
        e.preventDefault();
    });

    //

    $(document).on('click', '.play', function(){
        jQuery("iframe").each(function() {
            jQuery(this)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')});
    });
    //

    var now = new Date().getMonth();
    var nowDate = new Date().getDate();
    var nowMonth = 1;
    var timerMonth = 0;

    if(now < 1){
        nowMonth = 1;
    }else {
        nowMonth = now + 1;
    }

    if( nowDate >= 15){
        timerMonth = nowMonth +1;
    } else{
        timerMonth = nowMonth;
    }

    //

    if( nowDate >= 13 && nowDate <= 14){
        $('#participation-packages .up-price-stiker').css({'opacity' : '1' });

        $('#participation-packages .price').addClass('old-price');

        $('#participation-packages .new-price').css({'display' : 'block' });

        $('.on-pay').on('click', function (e) {
            e.preventDefault();
            var priceUpData = $(this).data('price-up-data');
            var priceUpSignature = $(this).data('price-up-signature');
            var priceUpRus = $(this).data('price-up-rus');
            $('.my-pay-form').find('input[name=data]').val(priceUpData);
            $('.my-pay-form').find('input[name=data]').val(priceUpData);
            $('.my-pay-form').find('input[name=signature]').val(priceUpSignature);
            $('.my-pay-formr, .my-pay-form2r, .my-pay-form3r').find('input[name=sum]').val(priceUpRus);
        });


    }else {
        $('#participation-packages .up-price-stiker').css({ 'opacity' : '0' });

        $('.on-pay').on('click', function (e) {
            e.preventDefault();
            var priceData = $(this).data('price-data');
            var priceSignature = $(this).data('price-signature');
            var priceRus = $(this).data('price-rus');
            $('.my-pay-form').find('input[name=data]').val(priceData);
            $('.my-pay-form').find('input[name=signature]').val(priceSignature);
            $('.my-pay-formr, .my-pay-form2r, .my-pay-form3r').find('input[name=sum]').val(priceRus);
        });
    }

    //

    $('#phone-control').on('click', function (e) {
       e.preventDefault();

       $('.phone-list').slideToggle(400);
       $(this).toggleClass('active-more-phone');
    });

    //

    if( nowMonth == 1 && nowDate <= 14 ){
        $('#monthStart').text('января');
    } else if( nowMonth == 1 && nowDate >= 14 ){
        $('#monthStart').text('февраля');
    } else if( nowMonth == 2 && nowDate <= 14 ){
        $('#monthStart').text('февраля');
    } else if( nowMonth == 2 && nowDate >= 14 ){
        $('#monthStart').text('марта');
    } else if( nowMonth == 3 && nowDate <= 14 ){
        $('#monthStart').text('марта');
    } else if( nowMonth == 3 && nowDate >= 14 ){
        $('#monthStart').text('апреля');
    } else if( nowMonth == 4 && nowDate <= 14 ){
        $('#monthStart').text('апреля');
    } else if( nowMonth == 4 && nowDate >= 14 ){
        $('#monthStart').text('мая');
    } else if( nowMonth == 5 && nowDate <= 14 ){
        $('#monthStart').text('мая');
    } else if( nowMonth == 5 && nowDate >= 14 ){
        $('#monthStart').text('июня');
    } else if( nowMonth == 6 && nowDate <= 14 ){
        $('#monthStart').text('июня');
    } else if( nowMonth == 6 && nowDate >= 14 ){
        $('#monthStart').text('июля');
    } else if( nowMonth == 7 && nowDate <= 14 ){
        $('#monthStart').text('июля');
    } else if( nowMonth == 7 && nowDate >= 14 ){
        $('#monthStart').text('августа');
    } else if( nowMonth == 8 && nowDate <= 14 ){
        $('#monthStart').text('августа');
    } else if( nowMonth == 8 && nowDate >= 14 ){
        $('#monthStart').text('сентября');
    } else if( nowMonth == 9 && nowDate <= 14 ){
        $('#monthStart').text('сентября');
    } else if( nowMonth == 9 && nowDate >= 14 ){
        $('#monthStart').text('октября');
    } else if( nowMonth == 10 && nowDate <= 14 ){
        $('#monthStart').text('октября');
    } else if( nowMonth == 10 && nowDate >= 14 ){
        $('#monthStart').text('ноября');
    } else if( nowMonth == 11 && nowDate <= 14 ){
        $('#monthStart').text('ноября');
    } else if( nowMonth == 11 && nowDate >= 14 ){
        $('#monthStart').text('декабря');
    } else if( nowMonth == 12 && nowDate <= 14 ){
        $('#monthStart').text('декабря');
    } else if( nowMonth == 12 && nowDate >= 14 ){
        $('#monthStart').text('января');
    }

    $(document).ready(function () {
        $('#timer-header').countdown('2018/' + timerMonth + '/15 00:00', function (event) {
            $(this).html(event.strftime(''
                + '<div class="v-center">%D<span class="after-number">Дней</span></div>'
                + '<div class="v-center">%H<span class="after-number">Часов</span></div>'
                + '<div class="v-center">%M<span class="after-number">Минут</span></div>'
                + '<div class="v-center">%S<span class="after-number">Секунд</span></div>'));
        });
    });

});

$(window).on('load', function () {

    'use strict';

    $('#loader').fadeOut(400);

});
