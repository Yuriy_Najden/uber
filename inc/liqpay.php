<?php

require_once __DIR__ . '/../LiqPay.php';

define('LIQPAY_PUBLIC_KEY', 'i86888406234');
define('LIQPAY_PRIVATE_KEY', '7An71hXHMskMxz95eIzq02ZaxOvJ0T2Owr1ufWeN');

function liqpay_form($params) {

    $liqpay = new LiqPay(LIQPAY_PUBLIC_KEY, LIQPAY_PRIVATE_KEY);

    $params['version'] = '3';
    $params['action'] = 'pay';
    $params['type'] = 'buy';
    $params['language'] = 'ru';
    $params['server_url'] = 'https://' . $_SERVER['HTTP_HOST'] . '/send2.php';
    $params['result_url'] = 'https://' . $_SERVER['HTTP_HOST'] . '/pay-thx.php';

    $out = $liqpay->cnb_form($params);

    return $out;
}

function liqpay_catch($data, $signature) {

    $sign = base64_encode( sha1(
        LIQPAY_PRIVATE_KEY .
        $data .
        LIQPAY_PRIVATE_KEY
        , 1
    ));

    if ($signature == $sign) {
        return @json_decode(base64_decode($data), true);
    } else {
        return false;
    }
}
