<?php

define('YANDEX_NOTIFICATION_SECRET', 'XXXXXX');

function yandex_catch() {

    $sign_arr = [
        $_POST['notification_type'],
        $_POST['operation_id'],
        $_POST['amount'],
        $_POST['currency'],
        $_POST['datetime'],
        $_POST['sender'],
        $_POST['notification_type'],
        YANDEX_NOTIFICATION_SECRET,
        $_POST['label']
    ];

    $sha1_hash = sha1(join('&', $sign_arr));

    if ($sha1_hash === $_POST['sha1_hash']) {
        return $_POST;
    } else {
        return false;
    }
}
