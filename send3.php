<?php

session_start();

require_once("classes/AmoCrm.php");

require_once ("vendor/autoload.php");

use PHPMailer\PHPMailer\PHPMailer;

/**
 * @param $data
 * @return string
 */
function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$name = clearData($_POST['name']);
$email = clearData($_POST['email']);
$question = clearData($_POST['question']);

$utmSource = clearData($_SESSION['utm_source']);
$utmMedium = clearData($_SESSION['utm_medium']);
$utmCampaign = clearData($_SESSION['utm_campaign']);
$utmTerm = clearData($_SESSION['utm_term']);
$utmContent = clearData($_SESSION['utm_content']);

if(!empty($name) && !empty($email)) {

    // Save user in crm
    $amoCrm = new AmoCrm([
        'USER_LOGIN' => 'info@projectx.fit',
        'USER_HASH'  => '17d8f036a67bd9a18945d718b8aa19fc'
    ], 'infoprojectxfit');

    $lead = $amoCrm->storeLead('Вопрос с сайта', 20081497, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent, $package, $instacaunt, $country, $question);

    $leadId = $lead['response']['leads']['add'][0]['id'];

    $amoCrm->storeContact($name, $leadId, $email, $phone);

    $mail = new PHPMailer();

    try {

        //Server settings
        $mail->isSMTP();
        $mail->Host = 'mail.adm.tools';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@projectx.fit';
        $mail->Password = '405E9tPh40us';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 25;
        $mail->CharSet = 'UTF-8';

        //Recipients
        $mail->setFrom('info@projectx.fit', 'info');
        $mail->addAddress('info@projectx.fit', 'info');

        //Content
        $mail->isHTML(true);
        $mail->Subject = 'Вопрос с сайта';
        $mail->Body = "<p>Имя: $name</p><p>Email: $email</p><p>Вопрос: $question</p>";

        $mail->send();

    } catch (Exception $e) {
        echo 'Message could not be sent.';

        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }

    /*$to = "projectxmarafon@gmail.com";
    $headers = "Content-type: text/plain; charset = windows-1251";
    $subject = 'Вопрос с сайта';
    $message = "Имя: $name \n Електронный адрес: $email \n Вопрос: $question";
    $send = mail ($to, $subject, $message)*/

    header('Location: result-thx.php');

} else {

    die('Data is empty!');

}