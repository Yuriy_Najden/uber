<?php

if (!defined('ABSPATH')) {
    exit();
}

function pay() {
    if (!wp_verify_nonce($_POST['_wpnonce'])) {
        die('Security error');
    }

    $public_key = 'i91820459928';
    $private_key = 'RLa5K9HLn2IKkW1Dlh3IlAXYVVwV46yitC33R9Oe';

    $liqpay = new LiqPay($public_key, $private_key);

    $number = date("YmdHis");

    $description_product = 'Оплата «' . $_POST['product_name'] . '». На этот E-Mail будет отправлен чек: '. $_POST['email'];


    $params = $liqpay->params([
        'action'         => 'pay',
        'amount'         => $_POST['product_price'],
        'currency'       => 'USD',
        'description'    => $description_product,
        'order_id'       => 'order_'.$number,
        'email'          => $_POST['email'],
        'version'        => '3',
        'result_url'	 => 'https://oksana-schmid.com/thx/'
    ]);

    $data = base64_encode(json_encode(array_merge(compact('public_key'), $params)));
    $signature = base64_encode(sha1($private_key.$data.$private_key, 1));

    $postfields  = http_build_query(array(
       'data'  => $data,
       'signature' => $signature
    ));

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://www.liqpay.com/api/3/checkout');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true); // Avoid MITM vulnerability http://phpsecurity.readthedocs.io/en/latest/Input-Validation.html#validation-of-input-sources
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);    // Check the existence of a common name and also verify that it matches the hostname provided
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    $server_output = curl_exec($ch);
    $information = curl_getinfo($ch);

    curl_close($ch);
    
    
    /*CRM AND MAILER LITE*/

    $groupsApi = (new \MailerLiteApi\MailerLite('59f8081e3aa7d0638cb41e27bdd9c774'))->groups();

    $subscriber = [
        'email' => $_POST['email'],
        'fields' => [
            'name' => $_POST['name'],
            'phone' => $_POST['phone'],
        ]
    ];

    $response = $groupsApi->addSubscriber(8031309, $subscriber);
    
    /*  **************************************    */    
 
 
     $crm = new AmoCRM('oksanaschmid', [
        'USER_LOGIN' => 'info@oksana-schmid.com',
        'USER_HASH' => '8dd73f6b7e383337d78fb0abd9678176'
    ]);

    $offer = $crm->createOffer($_POST['voronka'], $_POST['product_name']);



    $offer_id = $offer->response->leads->add[0]->id;

    $contact = $crm->createContact(
        $offer_id,
        $_POST['name'],
        $_POST['email'],
        $_POST['phone'],
        [
        	'utm_source' => $_SESSION['utm_source'] ? $_SESSION['utm_source'] : null,
        	'utm_medium' => $_SESSION['utm_medium'] ? $_SESSION['utm_medium'] : null,
        	'utm_campaign' => $_SESSION['utm_campaign'] ? $_SESSION['utm_campaign'] : null,
        	'utm_term' => $_SESSION['utm_term'] ? $_SESSION['utm_term'] : null,
        	'utm_content' => $_SESSION['utm_content'] ? $_SESSION['utm_content'] : null,
        ]
        
    );
    
    return header('Location: '.$information['redirect_url']);

}

add_action( 'admin_post_nopriv_pay', 'pay' );
add_action( 'admin_post_pay', 'pay' );


function prefix_send_email_to_admin() {
    if (!wp_verify_nonce($_POST['_wpnonce'])) {
        die('Security error');
    }

    $groupsApi = (new \MailerLiteApi\MailerLite('59f8081e3aa7d0638cb41e27bdd9c774'))->groups();

    $subscriber = [
        'email' => $_POST['email'],
        'fields' => [
            'name' => $_POST['name'],
            'phone' => $_POST['phone'],
        ]
    ];

    $response = $groupsApi->addSubscriber(8031309, $subscriber);


    /*  **************************************    */



    $crm = new AmoCRM('oksanaschmid', [
        'USER_LOGIN' => 'info@oksana-schmid.com',
        'USER_HASH' => '8dd73f6b7e383337d78fb0abd9678176'
    ]);

    $offer = $crm->createOffer($_POST['voronka'], $_POST['form_title']);



    $offer_id = $offer->response->leads->add[0]->id;

    $contact = $crm->createContact(
        $offer_id,
        $_POST['name'],
        $_POST['email'],
        $_POST['phone'],
        [
        	'utm_source' => $_SESSION['utm_source'] ? $_SESSION['utm_source'] : null,
        	'utm_medium' => $_SESSION['utm_medium'] ? $_SESSION['utm_medium'] : null,
        	'utm_campaign' => $_SESSION['utm_campaign'] ? $_SESSION['utm_campaign'] : null,
        	'utm_term' => $_SESSION['utm_term'] ? $_SESSION['utm_term'] : null,
        	'utm_content' => $_SESSION['utm_content'] ? $_SESSION['utm_content'] : null,
        ]
        
    );
    
    //var_dump($_SESSION);
	
	$redirect_title = str_replace(' ', '_', $_POST['form_title']);
	header('Location: /thx/?title='.$redirect_title);
    # redirect to ty page
}

add_action( 'admin_post_nopriv_contact_form', 'prefix_send_email_to_admin' );
add_action( 'admin_post_contact_form', 'prefix_send_email_to_admin' );
