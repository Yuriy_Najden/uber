<section class="prefooter">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <p>© 2018 Все права защищены.</p>
                <img src="img/logo.svg" alt="">
                <p>Разработано в <a href="https://smmstudio.com.ua/" target="_blank">SMMSTUDIO</a></p>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="content col-md-8 col-md-offset-2">
                <a href="#" data-toggle="modal" data-target="#pravicy">Политика конфиденциальности</a>
                <a href="#" data-toggle="modal" data-target="#renoun">Отказ от ответственности</a>
                <a href="#" data-toggle="modal" data-target="#dispatch">Согласие с рассылкой</a>
            </div>
        </div>
    </div>
</footer>

<?php include ('components/prav-politick.php');?>
<?php include ('components/renouncement.php');?>
<?php include ('components/dispatch.php');?>

<div class="modal fade popup-form" id="form-popup-pey-pac1" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Оплатить участие <br>
                    в марафоне

                </h4>
            </div>
            <div class="modal-body">
                <p class="cantry">Выберите страну:</p>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#pac1-ukr" data-toggle="tab"><span>Украина</span><br>(банковская карта)</a></li>
                    <li><a href="#pac1-rus" data-toggle="tab"><span>Россия</span> <br>(Яндекс.Деньги)</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="pac1-ukr">
                        <form role="form" method="post" class="my-pay-form" accept-charset="utf-8" action="https://www.liqpay.ua/api/3/checkout" >
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Имя" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control ukr-mask" name="phone" placeholder="Телефон" required>
                            </div>
                            <!--
                            <div class="form-group">
                                <input type="text" class="form-control" name="instacaunt" placeholder="Аккаунт участника в Instagram" required>
                            </div>
                            -->
                            <input type="hidden" name="package" value="пакет_Стандарт">
                            <input type="hidden" name="country" value="Украина">
                            <!-- <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiMzk5IiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6ItCj0YfQsNGB0YLQuNC1INCyINC80LDRgNCw0YTQvtC90LUgfCDQn9Cw0LrQtdGCINCh0KLQkNCd0JTQkNCg0KIiLCJ0eXBlIjoiYnV5IiwicmVzdWx0X3VybCI6Imh0dHBzOi8vcHJvamVjdHguZml0L3BheS10aHgucGhwIiwibGFuZ3VhZ2UiOiJydSJ9" />
                            <input type="hidden" name="signature" value="yW+zJN8/M+2Jm0qAhrHalCVnCGU=" />-->

                            <input type="hidden" name="data" value="" />
                            <input type="hidden" name="signature" value="" />

                            <!-- <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiMzk5IiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6ItCf0LDQutC10YIg0KHRgtCw0L3QtNCw0YDRgiIsInR5cGUiOiJidXkiLCJzZXJ2ZXJfdXJsIjoiaHR0cHM6Ly9wcm9qZWN0eC5maXQvc3VjY2Vzcy1wYXkucGhwIiwicmVzdWx0X3VybCI6Imh0dHBzOi8vcHJvamVjdHguZml0L3N1Y2Nlc3MtcGF5LnBocCIsImxhbmd1YWdlIjoicnUifQ==" />
                            <input type="hidden" name="signature" value="z4JZAB06mqOSD7AOjkBtLCNGKfA=" /> -->
                            
                            <!--
                            <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiMzk5IiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6ItGC0LXRgdGCIiwidHlwZSI6ImJ1eSIsInNhbmRib3giOiIxIiwic2VydmVyX3VybCI6Imh0dHBzOi8vd3d3LnByb2plY3R4LmZpdC9zdWNjZXNzLXBheS5waHAiLCJyZXN1bHRfdXJsIjoiaHR0cHM6Ly93d3cucHJvamVjdHguZml0L3N1Y2Nlc3MtcGF5LnBocCIsImxhbmd1YWdlIjoicnUifQ==" />
                             <input type="hidden" name="signature" value="g21OgreBJx1aG2V/UXCfdWZsNng=" />
                             -->



                            <button type="submit" class="button">перейти к оплате</button>
                            <p class="small">*После успешной оплаты, в открывшейся странице подтвердите свой
                                Instagram - аккаунт. Затем для Вас откроется доступ к марафону.
                            </p>
                        </form>
                    </div>
                    <div class="tab-pane" id="pac1-rus">
                        <form role="form" class="my-pay-formr" method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Имя" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="rus-mask form-control" name="phone" placeholder="Телефон" required>
                            </div>
                            <!--
                            <div class="form-group">
                                <input type="text" class="form-control" name="instacaunt" placeholder="Аккаунт участника в Instagram" required>
                            </div>
                            -->
                            <input type="hidden" name="package" value="пакет Стандарт">
                            <input type="hidden" name="country" value="Россия">
                            <input type="hidden" name="receiver" value="410014043888356">
                            <input type="hidden" name="formcomment" value="">
                            <input type="hidden" name="short-dest" value="">
                            <input type="hidden" name="label" value="$order_id">
                            <input type="hidden" name="quickpay-form" value="donate">
                            <input type="hidden" name="targets" value="транзакция {order_id}">
                            <input type="hidden" name="sum" value="" data-type="number">
                            <input type="hidden" name="need-fio" value="true">
                            <input type="hidden" name="need-email" value="true">
                            <input type="hidden" name="need-phone" value="true">
                            <input type="hidden" name="need-address" value="false">
                            <input type="hidden" name="successURL" value="https://projectx.fit/success-pay.php">
                            <label><input type="radio" name="paymentType" value="PC">Яндекс.Деньгами</label>
                            <label><input type="radio" name="paymentType" value="AC">Банковской картой</label>
                            <button type="submit" class="button">перейти к оплате</button>
                             <p class="small">*После успешной оплаты, в открывшейся странице подтвердите свой
                                Instagram - аккаунт. Затем для Вас откроется доступ к марафону.
                            </p>
                        </form>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade popup-form" id="form-popup-pey-pac2" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Оплатить участие <br>
                    в марафоне

                </h4>
            </div>
            <div class="modal-body">
                <p class="cantry">Выберите страну:</p>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#pac2-ukr" data-toggle="tab"><span>Украина</span><br>(банковская карта)</a></li>
                    <li><a href="#pac2-rus" data-toggle="tab"><span>Россия</span> <br>(Яндекс.Деньги)</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="pac2-ukr">
                        <form role="form" class="my-pay-form" method="post" accept-charset="utf-8" action="https://www.liqpay.ua/api/3/checkout">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Имя" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control ukr-mask" name="phone" placeholder="Телефон" required>
                            </div>
                            <!--
                            <div class="form-group">
                                <input type="text" class="form-control" name="instacaunt" placeholder="Аккаунт участника в Instagram" required>
                            </div>
                            -->
                            <input type="hidden" name="package" value="пакет_Премиум">

                            <input type="hidden" name="country" value="Украина">
                            <!-- <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiOTk5IiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6ItCf0LDQutC10YIg0J/RgNC10LzQuNGD0LwiLCJ0eXBlIjoiYnV5IiwicmVzdWx0X3VybCI6Imh0dHBzOi8vcHJvamVjdHguZml0L3BheS10aHgucGhwIiwibGFuZ3VhZ2UiOiJydSJ9" />
                            <input type="hidden" name="signature" value="9JvpXw7alS4HNhebi/E3wAOxl3E=" />-->

                            
                            <input type="hidden" name="data" value="" />
                            <input type="hidden" name="signature" value="" />
                            
                             <!-- <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiOTk5IiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6ItCf0LDQutC10YIg0J/RgNC10LzQuNGD0LwiLCJ0eXBlIjoiYnV5Iiwic2VydmVyX3VybCI6Imh0dHBzOi8vcHJvamVjdHguZml0L3N1Y2Nlc3MtcGF5LnBocCIsInJlc3VsdF91cmwiOiJodHRwczovL3Byb2plY3R4LmZpdC9zdWNjZXNzLXBheS5waHAiLCJsYW5ndWFnZSI6InJ1In0=" />
                            <input type="hidden" name="signature" value="6bFKho/GaFSoteSEosmAgwlXhCU=" /> -->

                            <!--
                            <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiOTk5IiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6ItCi0LXRgdGCIiwidHlwZSI6ImJ1eSIsInNhbmRib3giOiIxIiwic2VydmVyX3VybCI6Imh0dHBzOi8vd3d3LnByb2plY3R4LmZpdC9zdWNjZXNzLXBheS5waHAiLCJyZXN1bHRfdXJsIjoiaHR0cHM6Ly93d3cucHJvamVjdHguZml0L3N1Y2Nlc3MtcGF5LnBocCIsImxhbmd1YWdlIjoicnUifQ==" />
                            <input type="hidden" name="signature" value="qY0xc9pDM+vhX0Gelm21SmLNXOo=" />
                            -->


                            <button type="submit" class="button">перейти к оплате</button>
                             <p class="small">*После успешной оплаты, в открывшейся странице подтвердите свой
                                Instagram - аккаунт. Затем для Вас откроется доступ к марафону.
                            </p>
                        </form>
                    </div>
                    <div class="tab-pane" id="pac2-rus">
                        <form role="form" class="my-pay-form2r" method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Имя" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="rus-mask form-control" name="phone" placeholder="Телефон" required>
                            </div>
                            <!--
                            <div class="form-group">
                                <input type="text" class="form-control" name="instacaunt" placeholder="Аккаунт участника в Instagram" required>
                            </div>
                            -->
                            <input type="hidden" name="package" value="пакет Премиум">
                            <input type="hidden" name="country" value="Россия">
                            <input type="hidden" name="receiver" value="410014043888356">
                            <input type="hidden" name="formcomment" value="">
                            <input type="hidden" name="short-dest" value="">
                            <input type="hidden" name="label" value="$order_id">
                            <input type="hidden" name="quickpay-form" value="donate">
                            <input type="hidden" name="targets" value="транзакция {order_id}">
                            <input type="hidden" name="sum" value="" data-type="number">
                            <input type="hidden" name="need-fio" value="true">
                            <input type="hidden" name="need-email" value="true">
                            <input type="hidden" name="need-phone" value="true">
                            <input type="hidden" name="need-address" value="false">
                            <input type="hidden" name="successURL" value="https://projectx.fit/success-pay.php">
                            <label><input type="radio" name="paymentType" value="PC">Яндекс.Деньгами</label>
                            <label><input type="radio" name="paymentType" value="AC">Банковской картой</label>
                            <button type="submit" class="button">перейти к оплате</button>
                             <p class="small">*После успешной оплаты, в открывшейся странице подтвердите свой
                                Instagram - аккаунт. Затем для Вас откроется доступ к марафону.
                            </p>
                        </form>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade popup-form" id="form-popup-pey-pac3" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Оплатить участие <br>
                    в марафоне

                </h4>
            </div>
            <div class="modal-body">
                <p class="cantry">Выберите страну:</p>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#pac3-ukr" data-toggle="tab"><span>Украина</span><br>(банковская карта)</a></li>
                    <li><a href="#pac3-rus" data-toggle="tab"><span>Россия</span> <br>(Яндекс.Деньги)</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="pac3-ukr">
                        <form role="form" class="my-pay-form" method="post" accept-charset="utf-8" action="https://www.liqpay.ua/api/3/checkout">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Имя" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control ukr-mask" name="phone" placeholder="Телефон" required>
                            </div>
                            <!--
                            <div class="form-group">
                                <input type="text" class="form-control" name="instacaunt" placeholder="Аккаунт участника в Instagram" required>
                            </div>
                            -->
                            <input type="hidden" name="package" value="пакет_VIP">
                            <input type="hidden" name="country" value="Украина">
                            <!--<input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiMjQ5OSIsImN1cnJlbmN5IjoiVUFIIiwiZGVzY3JpcHRpb24iOiLQn9Cw0LrQtdGCIFZJUCIsInR5cGUiOiJidXkiLCJyZXN1bHRfdXJsIjoiaHR0cHM6Ly9wcm9qZWN0eC5maXQvcGF5LXRoeC5waHAiLCJsYW5ndWFnZSI6InJ1In0=" />
                            <input type="hidden" name="signature" value="8XZJm63RJR3/8VNNa3kVo1vE20I=" />-->

                            
                            <input type="hidden" name="data" value="" />
                            <input type="hidden" name="signature" value="" />

                            <!-- <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiMjk5OSIsImN1cnJlbmN5IjoiVUFIIiwiZGVzY3JpcHRpb24iOiLQn9Cw0LrQtdGCIFZJUCIsInR5cGUiOiJidXkiLCJzZXJ2ZXJfdXJsIjoiaHR0cHM6Ly9wcm9qZWN0eC5maXQvc3VjY2Vzcy1wYXkucGhwIiwicmVzdWx0X3VybCI6Imh0dHBzOi8vcHJvamVjdHguZml0L3N1Y2Nlc3MtcGF5LnBocCIsImxhbmd1YWdlIjoicnUifQ==" />
                            <input type="hidden" name="signature" value="uelBl3xW5LSXG+tFxOkOCcWdcos=" /> -->
                            

                            <!--
                            <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiMjk5OSIsImN1cnJlbmN5IjoiVUFIIiwiZGVzY3JpcHRpb24iOiLQotC10YHRgiIsInR5cGUiOiJidXkiLCJzYW5kYm94IjoiMSIsInNlcnZlcl91cmwiOiJodHRwczovL3d3dy5wcm9qZWN0eC5maXQvc3VjY2Vzcy1wYXkucGhwIiwicmVzdWx0X3VybCI6Imh0dHBzOi8vd3d3LnByb2plY3R4LmZpdC9zdWNjZXNzLXBheS5waHAiLCJsYW5ndWFnZSI6InJ1In0=" />
                            <input type="hidden" name="signature" value="J4It9kfl9pE0cwusPa4nZSJQYow=" />
                            -->

                            <button type="submit" class="button">перейти к оплате</button>
                             <p class="small">*После успешной оплаты, в открывшейся странице подтвердите свой
                                Instagram - аккаунт. Затем для Вас откроется доступ к марафону.
                            </p>
                        </form>
                    </div>
                    <div class="tab-pane" id="pac3-rus">
                        <form role="form" class="my-pay-form3r" method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Имя" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="rus-mask form-control" name="phone" placeholder="Телефон" required>
                            </div>
                            <!--
                            <div class="form-group">
                                <input type="text" class="form-control" name="instacaunt" placeholder="Аккаунт участника в Instagram" required>
                            </div>
                            -->
                            <input type="hidden" name="package" value="пакет VIP">
                            <input type="hidden" name="country" value="Россия">
                            <input type="hidden" name="receiver" value="410014043888356">
                            <input type="hidden" name="formcomment" value="">
                            <input type="hidden" name="short-dest" value="">
                            <input type="hidden" name="label" value="$order_id">
                            <input type="hidden" name="quickpay-form" value="donate">
                            <input type="hidden" name="targets" value="транзакция {order_id}">
                            <input type="hidden" name="sum" value="" data-type="number">
                            <input type="hidden" name="need-fio" value="true">
                            <input type="hidden" name="need-email" value="true">
                            <input type="hidden" name="need-phone" value="true">
                            <input type="hidden" name="need-address" value="false">
                            <input type="hidden" name="successURL" value="https://projectx.fit/success-pay.php">
                            <label><input type="radio" name="paymentType" value="PC">Яндекс.Деньгами</label>
                            <label><input type="radio" name="paymentType" value="AC">Банковской картой</label>
                            <button type="submit" class="button">перейти к оплате</button>
                             <p class="small">*После успешной оплаты, в открывшейся странице подтвердите свой
                                Instagram - аккаунт. Затем для Вас откроется доступ к марафону.
                            </p>
                        </form>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade popup-form" id="form-popup-booking" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Забронировать место <br>
                    в марафоне
                </h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="send.php">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Имя" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="number" class="m-phone form-control" name="phone" placeholder="Телефон" required>
                    </div>
                    <!--
                    <div class="form-group">
                        <input type="text" class="form-control" name="instacaunt" placeholder="Аккаунт участника в Instagram" required>
                    </div>
                    -->
                    <input type="hidden" name="package" value="">
                    <button type="submit" class="button booking">Отправить</button>
                    <p class="small">* С Вами свяжется наш менеджер и поможет оформить
                        заявку на участие в марафоне
                    </p>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade popup-form" id="form-popup-book" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Запрос на <br>скачивание книги</h4>
            </div>
            <div class="modal-body">
                <form id="boock-download" role="form" method="post" action="send5.php">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Имя" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="phone" placeholder="Телефон" required>
                    </div>
                    <button type="submit" class="button booking">Скачать</button>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

</div>

<!--[if IE]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 7]>
<script src="https://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
<![endif]-->
<!--[if lt IE 8]>
<script src="https://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="js/jquery-3.2.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/wow.min.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.countdown.js"></script>
<script src="js/youtube.js"></script>
<script src="js/js.js"></script>
<style>
    .m-phone {
        -moz-appearance: textfield;
    }
    .m-phone::-webkit-inner-spin-button {
        display: none;
    }
</style>
</body>
</html>