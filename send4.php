<?php

session_start();

require_once("classes/AmoCrm.php");

require_once("inc/liqpay.php");

require_once ("vendor/autoload.php");

use PHPMailer\PHPMailer\PHPMailer;

/**
 * @param $data
 * @return string
 */
function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$name = clearData($_POST['name']);
$phone = clearData($_POST['phone']);
$email = clearData($_POST['email']);
$package = clearData($_POST['package']);
$country = clearData($_POST['country']);

$utmSource = clearData($_SESSION['utm_source']);
$utmMedium = clearData($_SESSION['utm_medium']);
$utmCampaign = clearData($_SESSION['utm_campaign']);
$utmTerm = clearData($_SESSION['utm_term']);
$utmContent = clearData($_SESSION['utm_content']);

$day = (int)date('d');

if ($day >= 13 && $day <= 15) {

    $prices = [
        'Оплата пакет Стандарт' => 449,
        'Оплата пакет Премиум'  => 1999,
        'Оплата пакет VIP'      => 9999,
    ];

} else {

    $prices = [
        'Оплата пакет Стандарт' => 499,
        'Оплата пакет Премиум'  => 2100,
        'Оплата пакет VIP'      => 11999,
    ];
}

if(!empty($name) && !empty($phone)) {

//    // Save user in crm
//    $amoCrm = new AmoCrm([
//        'USER_LOGIN' => 'info@projectx.fit',
//        'USER_HASH'  => '17d8f036a67bd9a18945d718b8aa19fc'
//    ], 'infoprojectxfit');
//
//    $lead = $amoCrm->storeLead('Переход на оплату', 20081497, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent, $package, $instacaunt, $country, $question);
//
//    $leadId = $lead['response']['leads']['add'][0]['id'];
//
//    $amoCrm->storeContact($name, $leadId, $email, $phone);
//
//    $mail = new PHPMailer();
//
//    try {
//
//        //Server settings
//        $mail->isSMTP();
//        $mail->Host = 'mail.adm.tools';
//        $mail->SMTPAuth = true;
//        $mail->Username = 'info@projectx.fit';
//        $mail->Password = '405E9tPh40us';
//        $mail->SMTPSecure = 'tls';
//        $mail->Port = 25;
//        $mail->CharSet = 'UTF-8';
//
//        //Recipients
//        $mail->setFrom('info@projectx.fit', 'info');
//        $mail->addAddress('info@projectx.fit', 'info');
//
//        //Content
//        $mail->isHTML(true);
//        $mail->Subject = 'Переход на оплату';
//        $mail->Body = "<p>Имя: $name</p><p>Аккаунт в Instagram: $instacaunt</p><p>Пакет: $package</p><p>Email: $email</p><p>Телефон: $phone</p><p>Страна: $country</p>";
//
//        $mail->send();
//
//    } catch (Exception $e) {
//        echo 'Message could not be sent.';
//
//        echo 'Mailer Error: ' . $mail->ErrorInfo;
//    }
//
//    $to = "projectxmarafon@gmail.com";
//    $headers = "Content-type: text/plain; charset = windows-1251";
//    $subject = 'Переход на оплату';
//    $message = "Имя: $name \n Електронный адрес: $email \n Телефон: $phone \n Пакет: $package \n Страна: $country";
//    $send = mail ($to, $subject, $message);

    $rv = [
        'success'   => true,
        'form'      => liqpay_form([
            'amount'        => $prices[$package],
            'currency'      => 'UAH',
            'description'   => $package,
            'info'          => $package . '|' . $email . '|' . $phone . '|' . $country
        ])
    ];

} else {

    $rv = ['success' => false];
}

echo json_encode($rv);
