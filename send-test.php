<?php

/**
 * @param $data
 * @return string
 */
function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$name = clearData($_POST['name']);
$phone = clearData($_POST['phone']);
$email = clearData($_POST['email']);
$package = clearData($_POST['package']);
$country = clearData($_POST['country']);

setrawcookie('_nameX', $name, time() + 1800);
setrawcookie('_phoneX', $phone, time() + 1800);
setrawcookie('_emailX', $email, time() + 1800);
setrawcookie('_packageX', $package, time() + 1800);
setrawcookie('_countryX', $country, time() + 1800);

