<?php
session_start();

if (!empty($_GET['name'])) {

    $_SESSION['name'] = $_GET['name'];
}

if (!empty($_GET['phone'])) {

    $_SESSION['phone'] = $_GET['phone'];
}

if (!empty($_GET['email'])) {

    $_SESSION['email'] = $_GET['email'];
}

if (!empty($_GET['instacaunt'])) {

    $_SESSION['instacaunt'] = $_GET['instacaunt'];
}

if (!empty($_GET['package'])) {

    $_SESSION['package'] = $_GET['package'];
}
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
   
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="Онлайн-марафона по похудению" />
    <meta property="og:description" content="Онлайн-марафона по похудению" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="https://projectx.fit/img/og.jpg" />
    <meta property="og:image" content="https://projectx.fit/img/og.jpg" />

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

      <meta name="msapplication-TileColor" content="#ffffff">

      <meta name="theme-color" content="#ffffff">

    <title>projectX</title>


      <link rel="stylesheet" href="css/animate.css">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="fonts/fontawesom/css/font-awesome.min.css">
      <link type="text/css" rel="stylesheet" href="css/style.css"/>


      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-N6RRWBC');</script>
      <!-- End Google Tag Manager -->
      <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '168326307143044');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=168326307143044&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
      <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '466908383766477');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=466908383766477&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

  </head>
  <body>
    <div class="wrapper">
        <div class="preloader" id="loader"></div>
        <header>
            <div class="container">
                <div class="row">
                    <div class="content col-md-12">
                        <a href="#" class="logo">
                            <img src="img/logo.svg" alt="">
                        </a>
                        <div class="contacts">
                            <a href="tel:78123095799" class="phone"><img src="img/RU.svg" alt="">7 812 309 57 99</a>
                            <a href="https://www.instagram.com/official.projectx/" class="instagram" target="_blank"><img src="img/instagram.png" alt=""></a>
                            <div class="phone phone-dropdowns">
                                <img src="img/UA.svg" alt="">
                                <a href="tel:0800210976">0 800 21 09 76</a>
                                <p>Бесплатно по Украине</p>
                                <a href="#" class="arrow-down" id="phone-control"><img src="img/accardeon-arrows.png" alt=""></a>
                                <ul class="phone-list">
                                    <li><a href="tel:380671206912">38 067 120 69 12</a></li>
                                    <li><a href="tel:380935375516">38 093 537 55 16</a></li>
                                    <li><a href="tel:380443323014">38 044 332 30 14</a></li>
                                </ul>
                            </div>
                            
                        </div>
                        <a href="#participation-packages" class="button scroll-to">Принять участие <span>в марафоне</span></a>
                    </div>
                </div>
            </div>
        </header>

        <section class="mob-top-banner">
            <div class="container">
                <div class="row">
                    <div class="content col-md-12">
                        <div class="text-pic">
                            <img src="img/header-text.png" alt="" class="text">
                            <img src="img/top-x.png" alt="" class="img">
                        </div>

                        <img src="img/top-pic.png" alt="" class="pic">
                        <a href="#top-banner" class="arrow scroll-to"><i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="top-baner" id="top-banner">
            <div class="container">
                <div class="row">
                    <div class="content col-md-12">
                        <h3>Старт онлайн-марафона 15 <span id="monthStart">октября</span><br> длительность 22 дня</h3>
                        <div class="text-pic">
                            <img src="img/header-text.png" alt="" class="text">
                            <img src="img/top-x.png" alt="" class="img">
                        </div>
                        <h2>Мы поможем Вам стать желанной и счастливой</h2>
                        <div class="text">
                            <div class="list">
                                <p>Примите участие в марафоне, и Вы сможете:</p>
                                <ul>
                                    <li>Изменить своё тело</li>
                                    <li>Заработать деньги</li>
                                    <li>Получить  призы</li>
                                    <li>Стать частью команды Проекта Х</li>
                                    <img src="img/printing.svg" alt="" class="print">
                                </ul>
                                <a href="#participation-packages" class="button scroll-to">Принять участие в марафоне</a>
                                <p class="context">*Все результаты сугубо индивидуальны и зависят от особенностей организма</p>
                            </div>
                            <div class="timer">
                                <p>Идёт набор на участие в марафоне</p>
                                <h3><nobr>осталось</nobr> </h3>
                                <div id="timer-header" class="timer-body"></div>
                            </div>
                        </div>
                        <img src="img/top-pic.png" alt="" class="block-pic">
                    </div>
                </div>
            </div>
            <div class="bubble b-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble b-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble b-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble s-bubble"></div>
        </section>

        <section class="if-you">
            <div class="container">
                <div class="row">
                    <div class="content col-md-12">
                        <div class="img">
                            <img src="img/if-you-img.png" alt="">
                        </div>
                        <div class="text">
                            <h2 class="block-title">Этот марафон специально для Вас, если Вы</h2>
                            <ul>
                                <li>Чувствуете недостаток времени и сил, но мечтаете изменить себя.</li>
                                <li>Недавно родили ребёнка и набрали лишние килограммы.</li>
                                <li>Думаете, что в Вашем возрасте уже поздно изменить себя и своё тело.</li>
                                <li>Склонны к полноте и уже почти смирились с этим.</li>
                                <li>Устали от изнурительных диет и подсчётов калорий.</li>
                                <li>Махнули на себя рукой и живёте без надежды  дать себе шанс поменяться.</li>
                            </ul>
                            <a href="#participation-packages" class="button scroll-to">Принять участие в марафоне</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bubble m-bubble"></div>
            <div class="bubble b-bubble"></div>
            <div class="bubble b-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble b-bubble"></div>
        </section>

        <section class="for-you">
            <div class="container">
                <h2 class="wotermark">project Х</h2>
                <div class="row">
                    <div class="content col-md-12">
                        <div class="text">
                            <h2 class="block-title">Проект Х для Вас — это</h2>
                            <ul>
                                <li>Возможность быстро и без мучений сбросить лишние килограммы.</li>
                                <li>Перезагрузка своего образа жизни.</li>
                                <li>Возможность раскрыть свой потенциал.</li>
                                <li>Стимул любить и быть любимой.</li>
                                <li>Возможность повысить самооценку.</li>
                                <li>Шанс стать частью команды Проекта Х и достигать вместе новых вершин.</li>
                            </ul>
                            <a href="#participation-packages" class="button scroll-to">Принять участие в марафоне</a>
                            <p class="context">*Все результаты сугубо индивидуальны и зависят от особенностей организма</p>
                        </div>
                        <div class="img">
                            <img src="img/for-you-pic1.png" alt="" class="pic">
                            <img src="img/for-you-pic2.png" alt="" class="pic">
                            <img src="img/for-you-pic3.png" alt="" class="pic">
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="format-work">
            <div class="container">
                <h2 class="block-title">Формат проведения</h2>
                <div class="row">
                    <div class="content col-md-12">
                        <h3>Закрытая группа в Instagram.</h3>
                        <h3>Контроль результатов.</h3>
                        <h3>Тренировки не более 10 минут в день.</h3>
                        <h3>Уникальные техники и методики тренировок.</h3>
                        <h3>Рекомендации по питанию.</h3>
                        <h3>Мотивация и полезный контент.</h3>
                        <h3>Полная конфиденциальность.</h3>
                    </div>
                </div>
            </div>
        </section>

        <section class="prize">
            <div class="container">
                <img src="img/priz-pic-1.png" alt="" class="asinc-pic frukt">
                <img src="img/priz-pic-2.png" alt="" class="asinc-pic clock">
                <img src="img/priz-pic-3.png" alt="" class="asinc-pic kubok">
                <h2 class="block-title">Как всё будет проходить</h2>
                <div class="row">
                    <div class="item col-md-4 col-md-offset-4">
                        <p class="number">1</p>
                        <p class="text">Знакомимся и определяем цели.</p>
                    </div>
                </div>
                <div class="row inup">
                    <div class="item col-md-4 col-md-offset-8">
                        <p class="number">2</p>
                        <p class="text">Определяем отправную точку трансформации
                            Вашего тела,
                            то, где Вы находитесь сейчас и
                            к какому результату мы будем идти.

                        </p>
                    </div>
                </div>
                <div class="row inup">
                    <div class="item col-md-4 col-md-offset-2">
                        <p class="number">3</p>
                        <p class="text">Даём рекомендации по питанию.</p>
                    </div>
                </div>
                <div class="row inup">
                    <div class="item col-md-4 col-md-offset-6">
                        <p class="number">4</p>
                        <p class="text">Планируем тренировки и стартуем.</p>
                    </div>
                </div>
                <div class="row inup">
                    <div class="item col-md-4 col-md-offset-1">
                        <p class="number">5</p>
                        <p class="text">Мотивируем и выкладываем
                            интересный, полезный контент.</p>
                    </div>
                </div>
                <div class="row inup">
                    <div class="item col-md-4 col-md-offset-7">
                        <p class="number">6</p>
                        <p class="text">Общаемся и делимся результатами.</p>
                    </div>
                </div>
                <div class="row inup">
                    <div class="item col-md-4 col-md-offset-3">
                        <p class="number">7</p>
                        <p class="text">Анализируем результаты
                            участников и награждаем.</p>
                    </div>
                </div>
            </div>
            <div class="bubble b-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble b-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble b-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble s-bubble"></div>
            <div class="bubble m-bubble"></div>
            <div class="bubble b-bubble"></div>
        </section>

        <section class="all-places">
            <div class="container">
                <h2 class="block-title">Призы для участников марафона</h2>
                <div class="row">
                    <div class="place-wrapper col-md-12">
                        <div class="place">
                            <p class="wotermark">1</p>
                            <div class="text">
                                <h3 class="block-title">1 место</h3>
                                <p class="price">
                                    <span>3 000</span> грн <br> / <br>
                                    <span>6 000</span> руб
                                </p>
                            </div>
                        </div>
                        <div class="place">
                            <p class="wotermark">2</p>
                            <div class="text">
                                <h3 class="block-title">2 место</h3>
                                <p class="price">
                                    <span>2 000</span> грн <br> / <br>
                                    <span>4 000</span> руб
                                </p>
                            </div>
                        </div>
                        <div class="place">
                            <p class="wotermark">3</p>
                            <div class="text">
                                <h3 class="block-title">3 место</h3>
                                <p class="price">
                                    <span>1 000</span> грн <br> / <br>
                                    <span>2 000</span> руб
                                </p>
                            </div>
                        </div>
                        <div class="place">
                            <p class="wotermark">4-5</p>
                            <div class="text">
                                <h3 class="block-title">4-5 места</h3>
                                <p class="price">
                                    <span>500</span> грн <br> / <br>
                                    <span>1 000</span> руб
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="consolation-prize">
                        <p><span>6-7 места</span> <b>бесплатное</b> участие в следующем марафоне.</p>
                    </div>
                    <h3 class="more">Для участницы, которая сможет показать выдающийся результат,
                        предоставится возможность стать тренером команды Проекта Х.</h3>
                    <a href="#member-team" class="button scroll-to">подробнее</a>
                </div>
            </div>
        </section>

        <section class="results">
            <div class="container">
                <h2 class="block-title">Результаты предыдущих марафонов</h2>
                <div class="row">
                    <div class="res-wrapper col-md-12">
                        <a href="#" class="res-control prev"></a>
                        <a href="#" class="res-control next"></a>
                        <div class="slider-wrapper" id="res-slider">
                           <div class="slide">
                                <img src="pic/result/48.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/49.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/50.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/51.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/52.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/53.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/54.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/55.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/56.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/57.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/58.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/59.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/60.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/61.jpg" alt="">
                            </div>
                            <div class="slide">
                                <img src="pic/result/62.jpg" alt="">
                            </div>
                        </div>
                        <p class="context">*Все результаты сугубо индивидуальны и зависят от особенностей организма</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="marathon-coach">
            <div class="container">
                <div class="row">
                    <div class="content col-md-12">
                        <div class="coach-slider">
                            <div class="slider-body">
                                <div class="slide">
                                    <div class="coach-pic">
                                        <img src="pic/coach-1.jpg" alt="">
                                    </div>
                                    <div class="about-coach">
                                        <h2 class="wotermark">Тренер</h2>
                                        <h2 class="block-title">Илона Чернобай</h2>
                                        <h3>Сертифицированный тренер по CrossFit,
                                            мастер спорта по спортивной гимнастике,
                                            модель и счастливая мама.</h3>
                                        <p>«Год назад я родила и стала счастливой мамой, моё тело сильно изменилось. Я
                                            набрала более 20 кг. Сейчас я работаю над собой и помогаю другим девочкам
                                            поверить в себя. Хочу показать на своём примере, что для того, чтобы стать
                                            снова красивой нужно лишь немного постараться!»
                                        </p>
                                        <div class="progress-pic">
                                            <h3>Моя трансформация</h3>
                                            <div class="pic-wrapper">
                                                <div class="pic">
                                                    <img src="pic/coach-1-transform-pic-1.jpg" alt="">
                                                    <p class="description">до беременности</p>
                                                </div>
                                                <div class="pic">
                                                    <img src="pic/coach-1-transform-pic-2.jpg" alt="">
                                                    <p class="description">после родов</p>
                                                </div>
                                                <div class="pic">
                                                    <img src="pic/coach-1-transform-pic-3.jpg" alt="">
                                                    <p class="description">после 4-х марафонов</p>
                                                </div>
                                            </div>

                                        </div>

                                        <a href="#" class="button" data-toggle="modal" data-target="#form-popup-book">Скачать книгу «Моя трансформация»</a>

                                    </div>
                                </div>
                            </div>
                            <div class="nav-slider">
                                <div class="item"><img src="" alt=""></div>
                                <div class="item"><img src="" alt=""></div>
                                <div class="item"><img src="" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="participation-packages" id="participation-packages">
            <div class="container">
                <h2 class="block-title">Пакеты участия в марафоне</h2>
                <div class="up-price">
                    <p>С 13 числа повышение стоимости пакетов</p>
                </div>
                <div class="row">
                    <div class="content col-md-12">
                        <div class="item">
                            <h3 class="package-name">Стандарт</h3>
                            <h4 class="price">449 грн/1 199 руб</h4>
                            <h4 class="new-price">499 грн/1 299 руб</h4>
                            
                            <ul>
                                <li>ежедневные тренировки 21 день;</li>
                                <li>10 правил питания;</li>
                                <li>меню на день;</li>
                                <li>простые рецепты правильного питания;</li>
                                <li>контроль, мотивация, поддержка, общение;</li>
                                <li>онлайн тренировки в прямом эфире;</li>
                                <li>выработка полезных привычек;</li>
                                <li>полезная информация;</li>
                                <li>прямые эфиры;</li>
                                <li>секреты красоты;</li>
                                <li>возможность побороться за призы.</li>
                            </ul>
                            <a href="#" 
	                           class="button on-pay" 
	                           data-type="Оплата пакет Стандарт" 
	                           data-toggle="modal" 
	                           data-target="#form-popup-pey-pac1" 
	                           data-price-up-data='eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiNDk5IiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6ItCf0LDQutC10YIg0KHRgtCw0L3QtNCw0YDRgiIsInR5cGUiOiJidXkiLCJzZXJ2ZXJfdXJsIjoiaHR0cHM6Ly93d3cucHJvamVjdHguZml0L3N1Y2Nlc3MtcGF5LnBocCIsInJlc3VsdF91cmwiOiJodHRwczovL3d3dy5wcm9qZWN0eC5maXQvc3VjY2Vzcy1wYXkucGhwIiwibGFuZ3VhZ2UiOiJydSJ9'
	                           data-price-up-signature='pocmZ8nOrXTmU2mWGJuijX/BB50='
	                           data-price-up-rus='1299' 
	                           data-price-data='eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiNDQ5IiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6ItCf0LDQutC10YIg0KHRgtCw0L3QtNCw0YDRgiIsInR5cGUiOiJidXkiLCJzZXJ2ZXJfdXJsIjoiaHR0cHM6Ly93d3cucHJvamVjdHguZml0L3N1Y2Nlc3MtcGF5LnBocCIsInJlc3VsdF91cmwiOiJodHRwczovL3d3dy5wcm9qZWN0eC5maXQvc3VjY2Vzcy1wYXkucGhwIiwibGFuZ3VhZ2UiOiJydSJ9'
	                           data-price-signature='HfaPovKO16hMEIUE1k+jJHrs8f8='
	                           data-price-rus='1199'>Оплатить участие</a>
                            <a href="#" class="booking on-booking" data-type="Забронировать пакет Стандарт" data-toggle="modal" data-target="#form-popup-booking">Забронировать место <i class="fa fa-angle-right" aria-hidden="true"></i></a>

                        </div>
                        
                        <div class="item">
                            <h3 class="package-name">Премиум</h3>
                            <h4 class="price">1 099 грн/2 699 руб</h4>
                            <h4 class="new-price">1 199 грн/2 999 руб</h4>
                            
                            <ul>
                                <li>ежедневные тренировки 21 день;</li>
                                <li>10 правил питания;</li>
                                <li>меню на день;</li>
                                <li>простые рецепты правильного питания;</li>
                                <li>контроль, мотивация, поддержка, общение;</li>
                                <li>онлайн тренировки в прямом эфире;</li>
                                <li>выработка полезных привычек;</li>
                                <li>полезная информация;</li>
                                <li>прямые эфиры;</li>
                                <li>секреты красоты;</li>
                                <li>возможность побороться за призы;</li>
                                <li><strong>личная консультация;</strong></li>
                                <li><strong>индивидуальный подбор питания.</strong></li>
                            </ul>
                            <a href="#" 
	                           class="button on-pay" 
	                           data-type="Оплата пакет Премиум" 
	                           data-toggle="modal" 
	                           data-target="#form-popup-pey-pac2"
	                           data-price-up-data='eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiMTE5OSIsImN1cnJlbmN5IjoiVUFIIiwiZGVzY3JpcHRpb24iOiLQn9Cw0LrQtdGCINCf0YDQtdC80LjRg9C8IiwidHlwZSI6ImJ1eSIsInNlcnZlcl91cmwiOiJodHRwczovL3d3dy5wcm9qZWN0eC5maXQvc3VjY2Vzcy1wYXkucGhwIiwicmVzdWx0X3VybCI6Imh0dHBzOi8vd3d3LnByb2plY3R4LmZpdC9zdWNjZXNzLXBheS5waHAiLCJsYW5ndWFnZSI6InJ1In0='
	                           data-price-up-signature='uN7jDFTfLPKb+kmX/ux4VppXinc='
	                           data-price-up-rus='2999' 
	                           data-price-data='eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiMTA5OSIsImN1cnJlbmN5IjoiVUFIIiwiZGVzY3JpcHRpb24iOiLQn9Cw0LrQtdGCINCf0YDQtdC80LjRg9C8IiwidHlwZSI6ImJ1eSIsInNlcnZlcl91cmwiOiJodHRwczovL3d3dy5wcm9qZWN0eC5maXQvc3VjY2Vzcy1wYXkucGhwIiwicmVzdWx0X3VybCI6Imh0dHBzOi8vd3d3LnByb2plY3R4LmZpdC9zdWNjZXNzLXBheS5waHAiLCJsYW5ndWFnZSI6InJ1In0='
	                           data-price-signature='2f138eVzUWkiBp7M6HWoRZvo0CQ='
	                           data-price-rus='2699'>Оплатить участие</a>
                            <a href="#" class="booking on-booking" data-type="Забронировать пакет Премиум" data-toggle="modal" data-target="#form-popup-booking">Забронировать место <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>

                        <div class="item">
                            <h3 class="package-name">VIP</h3>
                            <h4 class="price">9 999 грн/23 999 руб</h4>
                            <h4 class="new-price">10 999 грн/26 399 руб</h4>
                            
                            <ul>
                                <li>ежедневные тренировки 21 день;</li>
                                <li>10 правил питания;</li>
                                <li>простые рецепты правильного питания;</li>
                                <li>контроль, мотивация, поддержка, общение;</li>
                                <li>онлайн тренировки;</li>
                                <li>полезная информация;</li>
                                <!--<li>прямые эфиры;</li>-->
                                <li><strong>индивидуальное ведение 22 дня (Telegram);</strong></li>
                                <li><strong>личная консультация;</strong></li>
                                <li><strong>индивидуальный подбор питания;</strong></li>
                                <li><strong>составление индивидуальных тренировок (отталкиваясь от поставленных целей);</strong></li>
                                <li><strong>индивидуальные онлайн тренировки.</strong></li>
                            </ul>
                            <a href="#" 
	                           class="button on-pay" 
	                           data-type="Оплата пакет VIP" 
	                           data-toggle="modal" 
	                           data-target="#form-popup-pey-pac3"
	                           data-price-up-data='eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiMTE5OTkiLCJjdXJyZW5jeSI6IlVBSCIsImRlc2NyaXB0aW9uIjoi0J/QsNC60LXRgiBWSVAiLCJ0eXBlIjoiYnV5Iiwic2VydmVyX3VybCI6Imh0dHBzOi8vd3d3LnByb2plY3R4LmZpdC9zdWNjZXNzLXBheS5waHAiLCJyZXN1bHRfdXJsIjoiaHR0cHM6Ly93d3cucHJvamVjdHguZml0L3N1Y2Nlc3MtcGF5LnBocCIsImxhbmd1YWdlIjoicnUifQ=='
	                           data-price-up-signature='Au8DfUpjPnDp5Km1EPmInVwNVnE='
	                           data-price-up-rus='26399' 
	                           data-price-data='eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTg2ODg4NDA2MjM0IiwiYW1vdW50IjoiOTk5OSIsImN1cnJlbmN5IjoiVUFIIiwiZGVzY3JpcHRpb24iOiLQn9Cw0LrQtdGCIFZJUCIsInR5cGUiOiJidXkiLCJzZXJ2ZXJfdXJsIjoiaHR0cHM6Ly93d3cucHJvamVjdHguZml0L3N1Y2Nlc3MtcGF5LnBocCIsInJlc3VsdF91cmwiOiJodHRwczovL3d3dy5wcm9qZWN0eC5maXQvc3VjY2Vzcy1wYXkucGhwIiwibGFuZ3VhZ2UiOiJydSJ9'
	                           data-price-signature='7o11dKhYZZLy6cCznLcSc+RImU8='
	                           data-price-rus='23999'>Оплатить участие</a>
                            <a href="#" class="booking on-booking" data-type="Забронировать пакет VIP" data-toggle="modal" data-target="#form-popup-booking">Забронировать место <i class="fa fa-angle-right" aria-hidden="true"></i></a>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--
        <section class="additional-options">
            <div class="container">
                <div class="row">
                    <div class="about-options col-md-6">
                        <h2 class="block-title">Дополнительные опции, которые сделают марафон ещё более эффективным</h2>
                        <p>Специально разработанные комплексы для макимально эффективной коррекции конкретной части
                            тела, которые дополительно включаются в общий марафон и совместно дают просто невероятный
                            эффект.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="content col-md-12">
                        <div class="option col-md-9">
                            <div class="option-container">
                                <div class="price">
                                    <h2 class="block-title">Ягодицы</h2>
                                    <div class="price-type">
                                        <h4>Старт марафона 18 марта</h4>
                                        <p>399 грн/799 руб</p>
                                    </div>
                                </div>
                                <div class="about-option">
                                    <ul>
                                        <li>тренировки с упором на ягодицы;</li>
                                        <li>10 правил питания для красивых ягодиц;</li>
                                        <li>простые рецепты правильного питания;</li>
                                        <li>контроль, мотивация, поддержка;</li>
                                        <li>онлайн тренировки в прямом эфире;</li>
                                        <li>выработка полезных привычек;</li>
                                        <li>полезная информация;</li>
                                        <li>прямые эфиры.</li>
                                    </ul>
                                </div>
                            </div>
                            <img src="img/option-1-pic.png" alt="" class="option-pic">
                            <a href="#" class="button on-pay" data-type="Оплата опцию Ягодицы" data-toggle="modal" data-target="#form-popup-pey-pac1">Оплатить участие</a>
                            <a href="#" class="booking on-booking" data-type="Забролнировать опцию Ягодицы" data-toggle="modal" data-target="#form-popup-booking">Забронировать место <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="option col-md-9 col-md-offset-3">
                            <div class="option-container">
                                <div class="price">
                                    <h2 class="block-title">Брюшной пресс</h2>
                                    <div class="price-type">
                                        <h4>Старт марафона 20 марта</h4>
                                        <p>399 грн/799 руб</p>
                                    </div>
                                </div>
                                <div class="about-option">
                                    <ul>
                                        <li>тренировки с упором на брюшной пресс;</li>
                                        <li>10 правил питания для прорисовки пресса;</li>
                                        <li>простые рецепты правильного питания;</li>
                                        <li>контроль, мотивация, поддержка;</li>
                                        <li>онлайн тренировки в прямом эфире;</li>
                                        <li>выработка полезных привычек;</li>
                                        <li>полезная информация;</li>
                                        <li>прямые эфиры.</li>
                                    </ul>
                                </div>
                            </div>
                            <img src="img/option-2-pic.png" alt="" class="option-pic">
                            <a href="#" class="button on-pay" data-type="Оплата опцию Брюшной пресс" data-toggle="modal" data-target="#form-popup-pey-pac1">Оплатить участие</a>
                            <a href="#" class="booking on-booking" data-type="Забролнировать опцию Брюшной пресс" data-toggle="modal" data-target="#form-popup-booking">Забронировать место <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div
                    </div>
                </div>
            </div>
        </section>
        -->

        <section class="about">
            <div class="container">
                <h2 class="block-title">О Проекте Х</h2>
                <div class="row">
                    <div class="content col-md-10 col-md-offset-1">
                        <div class="video-wrapper">

                            <iframe src="https://www.youtube.com/embed/BwPhgXyfavs?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                            

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="reviews">
            <div class="container">
                <h2 class="block-title">Отзывы</h2>
                <div class="row">
                    <div class="content col-md-12">
                        <a href="#" class="rev-controls prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                        <a href="#" class="rev-controls next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        <div class="rev-slider" id="rev-slider">
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="LmY3sh43rDA"></div>
                                </div>
                                <div class="img">
                                    <img src="pic/rev-1.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="SE1QES8qVf8"></div>
                                </div>
                                <div class="img">
                                    <img src="pic/rev-2.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="AT0WJuXK9Gc"></div>
                                </div>
                                <div class="img">
                                    <img src="pic/rev-4.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="U_SfKGNzsdI"></div>
                                </div>
                                <div class="img">
                                    <img src="pic/rev-3.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="MiymKLm0I9A"></div>
                                </div>
                                <div class="img">
                                    <img src="pic/rev-5.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="mLWxH8HZNy8"></div>
                                </div>
                                <div class="img">
                                    <img src="pic/rev-6.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="V0O_HPl6ya4"></div>
                                </div>
                                <div class="img">
                                    <img src="pic/rev-7.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="WhypKDuOX7Y"></div>
                                </div>
                                <div class="img">
                                    <img src="pic/rev-9.jpg" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="mob-rev-slider" id="mob-rev-slider">
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="LmY3sh43rDA"></div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="img">
                                    <img src="pic/rev-1.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="SE1QES8qVf8"></div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="img">
                                    <img src="pic/rev-2.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="AT0WJuXK9Gc"></div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="img">
                                    <img src="pic/rev-4.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="U_SfKGNzsdI"></div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="img">
                                    <img src="pic/rev-3.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="MiymKLm0I9A"></div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="img">
                                    <img src="pic/rev-5.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="mLWxH8HZNy8"></div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="img">
                                    <img src="pic/rev-6.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="V0O_HPl6ya4"></div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="img">
                                    <img src="pic/rev-7.jpg" alt="">
                                </div>
                            </div>
                            <div class="slide">
                                <div class="video">
                                    <div class="youtube" id="WhypKDuOX7Y"></div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="img">
                                    <img src="pic/rev-9.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <p class="context">*Все результаты сугубо индивидуальны и зависят от особенностей организма</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="faq">
            <div class="container">
                <h2 class="block-title">Часто задаваемые вопросы</h2>
                <div class="row">
                    <div class="content col-md-6 col-md-offset-3">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#faq1">
                                            Общие вопросы
                                        </a>
                                    </h4>
                                </div>
                                <div id="faq1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-1">В чем суть марафона?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-1" class="collapse">Марафон длится 22 дня. Первый день — информационный
                                            и 21 день работы над собой. Марафон включает в себя: физнагрузки, даёт 10 основных
                                            правил питания, следуя которым можно не беспокоиться о подсчете ккал, взвешивании
                                            еды и диетах. Помимо этого, Вас ждет много полезной информации, мотивации, прямых
                                            эфиров, онлайн тренировок и т.д. Все происходит в закрытом аккаунте Инстаграмм,
                                            куда Вас добавляют после регистрации.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-2">Что такое «Подготовительная неделя» и как она проходит?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-2" class="collapse">Перед стартом марафонов «Стандарт» и «Премиум» есть
                                            «Подготовительная неделя». Она начинается за 7 дней до официального старта марафона.
                                            В нее автоматически попадают все зарегистрированные участницы. Неделя нужна тем,
                                            кто давно не занимался спортом или сомневается в своих силах. С помощью простых
                                            упражнений мы плавно подведем Вас к старту. Чем раньше Вы оплатите, тем лучше
                                            сможете подготовиться.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-3">Можно ли мне участвовать, если я кормлю грудью?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-3" class="collapse">Можно. Кормящие мамы могут заниматься спортом. У
                                            нас нет диет, поэтому питание никаким образом не повлияет на лактацию. Рекомендуем
                                            приобрести хороший топ для фиксации груди.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-4">Какие противопоказания к участию?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-4" class="collapse">Если у Вас есть сомнения по поводу собственного
                                            здоровья, давние травмы, диастаз, недавно были роды и т.д., разрешение на
                                            тренировки Вам может дать только врач при личном осмотре.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-5">Я недавно родила. Когда я могу приступить к марафону?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-5" class="collapse">Все индивидуально. В среднем, после естественных
                                            родов без травм и разрывов, начинать тренироваться можно через 8 недель. После
                                            кесарево интенсивные тренировки разрешены минимум через 8 месяцев. Проконсультируйтесь
                                            со своим личным гинекологом.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-6">Смогу ли я заниматься, если у меня много лишнего веса/ нет лишнего веса, но я хочу улучшить качество тела?<i class="fa fa-angle-right" aria-hidden="true"></i>
                                        </a>
                                        <p id="faq1-6" class="collapse">Мы тренируем по системе Табата, которая подстраивается
                                            под Вас, а не Вы под нее. Выполнять тренировку смогут люди с абсолютно разной
                                            физподготовкой. Кроме того для Вас есть «Подготовительная неделя». Тренировки
                                            по системе Табата, отлично убирают целлюлит и подтягивают тело.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-7">Как принять участие в борьбе за призы?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-7" class="collapse">Конкурсы с призами есть в марафонах «Стандарт» и
                                            «Премиум». Для участия Вам необходимо сделать фото ДО/ПОСЛЕ, по образцу и раз
                                            в неделю отчитываться о изменениях в параметрах лично тренеру.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-8">Нужно ли мне выкладывать на свою страничку фото До/После или тренировки?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-8" class="collapse">Активность на своей странице — по желанию. На
                                            результаты конкурса это не влияет.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-9">Я не хочу чтобы кто-то знал что я худею, что делать?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-9" class="collapse">Вы можете принимать участие анонимно, создав новый
                                            аккаунт для марафона, либо не вести активность на своей странице. Все фото и
                                            отчеты будут отправляться лично тренеру.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-10">Что будет со страницей марафона после окончания?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-10" class="collapse">Спустя 3 дня, аккаунт удаляется. Вся информация в аккаунте — наша интеллектуальная собственность.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-11">Можно ли принимать участие в марафоне жителям других стран?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-11" class="collapse">Да.</p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-12">Нужна ли специальная подготовка для участия в проекте?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-12" class="collapse">Нет.</p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-13">Если не с кем оставить маленького ребенка, я смогу участвовать?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-13" class="collapse">Да! Программа тренировок составлена так, что Вы
                                            сможете выполнять задания в любых условиях и это не займет много времени.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-14">Можно ли не претендовать на призы, а учавствовать в марафоне для себя?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-14" class="collapse">Можно. В таком случае Вам не нужно присылать фото До/После и отчеты.</p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-15">Как и кем определяется победитель?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-15" class="collapse">Победитель определяется по результатам марафона,
                                            исходя из многих критериев. Самый основной — это Ваши изменения. Победителя
                                            определяет Ваш тренер, который прошёл вместе с Вами весь марафон.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-16">По каким причинам я могу выбыть из борьбы за призы?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-16" class="collapse">Вы не сможете претендовать на призы, если не прислали
                                            фото До/После и еженедельные отчеты в виде параметров и веса.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq1-17">Могу ли я дополнительно посещать тренировки в спортивном зале?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq1-17" class="collapse">На Ваше усмотрение.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#faq2">
                                            Вопросы по питанию
                                        </a>
                                    </h4>
                                </div>
                                <div id="faq2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq2-1">Где покупать продукты для диеты?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq2-1" class="collapse">Диет у нас не будет! Будут лишь 10 правил питания и примерное меню на день, которому Вы
                                            можете следовать. Все продукты самые обычные, которые можно купить в любом магазине.
                                        </p>

                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq2-2">Как я узнаю, что мне есть? Будут ли расписаны приемы пищи? Если я на ГВ, что мне есть?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq2-2" class="collapse">В начале марафона Вам будет представлено 10 золотых правил питания, прочитав которые Вы
                                            сразу поймете что и как кушать. Также, ежедневно для Вас будет примерное меню, которого
                                            Вы можете строго придерживаться, либо отклоняться в рамках 10 правил. Грудное вскармливание
                                            отлично сочетается с нашими правилами.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#faq3">
                                            Вопросы по тренировкам
                                        </a>
                                    </h4>
                                </div>
                                <div id="faq3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq3-1">Как будут проходить тренировки?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq3-1" class="collapse">Все задания будут даваться за день до выполнения, в виде видео-тренировки. Также для каждого
                                            упражнения будет подробное видео с правильной техникой. Выполнять можно в любое время суток.
                                            Тренировки предназначены для выполнения в домашних условиях.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq3-2">Сколько тренировок в неделю?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq3-2" class="collapse">6 тренировок в неделю. Каждое воскресенье — день отчета и выходной от занятий.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq3-3">Сколько времени длятся тренировки?<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <p id="faq3-3" class="collapse">До 15 мин</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#faq4">
                                            Оплата участия в марафоне
                                        </a>
                                    </h4>
                                </div>
                                <div id="faq4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq4-1"> Можно ли поменять пакет участия в
                                            марафоне?<i class="fa fa-angle-right" aria-hidden="true"></i>
                                        </a>
                                        <p id="faq4-1" class="collapse">Поменять пакет участник может до оплаты.
                                            После оплаты пакет изменить нельзя.
                                        </p>
                                        <a href="#" data-toggle="collapse" class="q-title collapsed" data-target="#faq4-2">Как я смогу оплатить участие в проекте, если не проживаю в Украине?<i class="fa fa-angle-right" aria-hidden="true"></i>
                                        </a>
                                        <p id="faq4-2" class="collapse">С помощью: VISA, MASTERCARD, LIQPAY, Яндекс Деньги.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="member-team" id="member-team">
            <div class="container">
                <div class="row">
                    <div class="content col-md-11 col-md-offset-1">
                        <div class="text">
                            <h2 class="block-title">Возможность стать членом команды Проекта Х</h2>
                            <p>Участвуя в проекте, Вы имеете уникальный шанс стать членом нашей команды!</p>
                            <h4>Для этого Вам необходимо:</h4>
                            <ul>
                                <li>Принять участие в марафоне и пройти свой путь похудения вместе с Проектом Х.</li>
                                <li>Быть активной (выкладывать интересные посты и участвовать в обсуждении на страничке
                                    марафона и своей личной).
                                </li>
                                <li>Быть примером для других девочек, в плане работы над собой и своим телом.</li>
                                <li>Иметь огромное желание.</li>
                            </ul>
                            <a href="#participation-packages" class="button scroll-to">Принять участие в марафоне</a>
                        </div>
                        <div class="img">
                            <img src="img/x-botton.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="feedback-form">
            <div class="container">
                <div class="row">
                    <div class="content col-md-11">
                        <h1 class="slogan">Измени свою
                            жизнь — начни
                            прямо сейчас!
                        </h1>
                        <div class="form">
                            <h2 class="block-title">Остались вопросы? <br>С радостью Вам ответим!</h2>
                            <form role="form" method="post" action="send3.php">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Имя" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="form-group form-textarea">
                                    <textarea name="question" placeholder="Вопрос" required></textarea>
                                </div>

                                <button type="submit" class="button">Отправить</button>
                                <p class="context">*Все результаты сугубо индивидуальны и зависят от особенностей организма</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php include ('footer.php');?>