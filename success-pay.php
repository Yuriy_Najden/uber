<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!--<meta property="og:title" content="Онлайн-марафона по похудению" />
    <meta property="og:description" content="Онлайн-марафона по похудению" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="https://projectx.fit/img/og.jpg" />
    <meta property="og:image" content="https://projectx.fit/img/og.jpg" />-->

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">

    <meta name="theme-color" content="#ffffff">

    <title>Спасибо</title>

    <link rel="stylesheet" href="css/likely.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css"/>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N6RRWBC');</script>
    <!-- End Google Tag Manager -->

</head>
<body>
<div class="wrapper">
    <section class="thx-body">
        <div class="container">
            <div class="row">
                <div class="content col-md-12">
                    <h2>Спасибо!</h2>
                    <p>В течение рабочего дня с Вами свяжется менеджер и расскажет дальнейшие действия.</p>
                    <form method="post" >
                        <input type="hidden" name="package" value="<?php echo $_COOKIE['packageX']?>">
                        <input type="hidden" name="email" value="<?php echo $_COOKIE['emailX']?>">
                        <input type="hidden" name="phone" value="<?php echo $_COOKIE['phoneX']?>">
                        <input type="hidden" name="name" value="<?php echo $_COOKIE['nameX']?>">
                        <input type="hidden" name="country" value="<?php echo $_COOKIE['countryX']?>">
                    </form>
                    <img src="img/thx-x.png" alt="" class="x-pic">
                </div>
            </div>
        </div>
    </section>
    <section class="prefooter">
        <div class="container">
            <div class="row">
                <div class="content col-md-12">
                    <p>© 2018 Все права защищены.</p>
                    <img src="img/logo.svg" alt="">
                    <p>Разработано в <a href="https://smmstudio.com.ua/" target="_blank">SMMSTUDIO</a></p>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="content col-md-8 col-md-offset-2">
                    <a href="#" data-toggle="modal" data-target="#pravicy">Политика конфиденциальности</a>
                    <a href="#" data-toggle="modal" data-target="#renoun">Отказ от ответственности</a>
                    <a href="#" data-toggle="modal" data-target="#dispatch">Согласие с рассылкой</a>
                </div>
            </div>
        </div>
    </footer>

</div>

<!--[if IE]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 7]>
<script src="https://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
<![endif]-->
<!--[if lt IE 8]>
<script src="https://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="js/jquery-3.2.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/wow.min.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.countdown.js"></script>
<script src="js/youtube.js"></script>
<script src="js/js.js"></script>
<!-- <script>
    $( document ).ready(function() {
        $('#confirmation-pay').submit();
    });
</script> -->
</body>
</html>
